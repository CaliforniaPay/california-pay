package com.linln.admin.personMch.consts;

/**
 * Created by thinkpad on 2019/1/5.
 */
public enum AccountAttr {
    PUBLIC("对公"),
    PRIVATE("对私");

    public String desc;

    AccountAttr(String desc){
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
