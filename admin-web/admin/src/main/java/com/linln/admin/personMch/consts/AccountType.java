package com.linln.admin.personMch.consts;

/**
 * Created by thinkpad on 2019/1/5.
 */
public enum AccountType {
    BANK("银行账户");

    public String desc;

    AccountType(String desc){
        this.desc = desc;
    }

}
