package com.linln.admin.personMch.consts;

/**
 * Created by thinkpad on 2019/1/5.
 */
public enum SettleStatus {
    AUDIT("审核中"),
    PASS("审核通过"),
    NO_PASS("审核不通过"),
    DONE("打款成功");

    public String desc;

    SettleStatus(String desc){
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
