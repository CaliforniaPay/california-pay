package com.linln.admin.personMch.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.alibaba.dubbo.config.annotation.Reference;
import com.california.pay.facade.MchOrderPayFacade;
import com.california.pay.result.CommonResult;
import com.linln.admin.core.constant.AdminConst;
import com.linln.admin.core.enums.ResultEnum;
import com.linln.admin.core.exception.ResultException;
import com.linln.admin.core.shiro.ShiroUtil;
import com.linln.admin.personMch.domain.PayOrder;
import com.linln.admin.personMch.service.PayOrderService;
import com.linln.core.utils.ResultVoUtil;
import com.linln.core.vo.ResultVo;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * Created by thinkpad on 2018/12/22.
 */
@Controller
@RequestMapping("/payOrder")
public class PayOrderController {
    @Autowired
    private PayOrderService payOrderService;

    @Reference
    private MchOrderPayFacade mchOrderPayFacade;
    /**
     * 列表页面
     */
    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("/payOrder/index")
    public String index() {
        return "/payOrder/index";
    }

    @GetMapping("/data")
    @ResponseBody
    public ResultVo data(Model model, PayOrder payOrder,
                          @RequestParam(value="page",defaultValue="1") int pageIndex,
                          @RequestParam(value="size",defaultValue="10") int pageSize){
        //目前全部以管理员判断
        if(!ShiroUtil.getSubject().getId().equals(AdminConst.ADMIN_ID)){
            payOrder.setInnerMchNo(ShiroUtil.getSubject().getMchInfo().getMchNo());
        }
        Page<PayOrder> list = payOrderService.getPageList(payOrder, pageIndex, pageSize);
        return ResultVoUtil.success("加载数据完成！", list);
    }

    @GetMapping("/exportData")
    public void exportData(Model model, PayOrder payOrder, HttpServletResponse response){
        //目前全部以管理员判断
        if(!ShiroUtil.getSubject().getId().equals(AdminConst.ADMIN_ID)){
            payOrder.setInnerMchNo(ShiroUtil.getSubject().getMchInfo().getMchNo());
        }
        List<PayOrder> list = payOrderService.getList(payOrder);
        ExportParams exportParams = new ExportParams();
        exportParams.setTitle("订单列表导出");
        try {
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=" +
                    URLEncoder.encode(DateFormatUtils.format(new Date(),"yyyyMMddHHssmmSSS")+".xls", "UTF-8"));
            ExcelExportUtil.exportExcel(exportParams, PayOrder.class, list).write(response.getOutputStream());
        } catch (IOException e) {
            throw new ResultException(ResultEnum.STATUS_ERROR);
        }
    }

    @PostMapping("/dealFailed")
    @ResponseBody
    public ResultVo dealFailed(Model model, PayOrder payOrder){
        // 不允许操作超级管理员数据
        if (!ShiroUtil.getSubject().getId().equals(AdminConst.ADMIN_ID)) {
            throw new ResultException(ResultEnum.NO_ADMIN_AUTH);
        }
        CommonResult<?> result = mchOrderPayFacade.supplyPayOrder(payOrder.getOrderId());
        if(result.isSuccess()){
            return ResultVoUtil.success("手动补单完成！");
        }else{
            return ResultVoUtil.error("补单失败："+result.getErrmsg());
        }
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    public String toDetail(@PathVariable("id") String id, Model model){
        PayOrder payOrder = payOrderService.getById(id);
        model.addAttribute("payOrder",payOrder);
        return "/payOrder/detail";
    }
}
