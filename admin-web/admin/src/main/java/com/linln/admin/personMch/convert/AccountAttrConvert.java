package com.linln.admin.personMch.convert;

import com.linln.admin.personMch.consts.AccountAttr;
import com.linln.admin.personMch.consts.SettleStatus;

import javax.persistence.AttributeConverter;

/**
 * Created by thinkpad on 2019/1/4.
 */
public class AccountAttrConvert implements AttributeConverter<AccountAttr, String> {
    @Override
    public String convertToDatabaseColumn(AccountAttr status) {
        return status.name();
    }

    @Override
    public AccountAttr convertToEntityAttribute(String s) {
        return AccountAttr.valueOf(s);
    }
}
