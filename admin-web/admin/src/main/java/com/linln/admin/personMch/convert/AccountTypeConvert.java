package com.linln.admin.personMch.convert;

import com.linln.admin.personMch.consts.AccountType;
import com.linln.admin.personMch.consts.SettleStatus;

import javax.persistence.AttributeConverter;

/**
 * Created by thinkpad on 2019/1/4.
 */
public class AccountTypeConvert implements AttributeConverter<AccountType, String> {
    @Override
    public String convertToDatabaseColumn(AccountType status) {
        return status.name();
    }

    @Override
    public AccountType convertToEntityAttribute(String s) {
        return AccountType.valueOf(s);
    }
}
