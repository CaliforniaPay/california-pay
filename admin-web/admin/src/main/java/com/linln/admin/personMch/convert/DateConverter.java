package com.linln.admin.personMch.convert;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import javax.persistence.AttributeConverter;
import java.util.Date;

public class DateConverter implements AttributeConverter<String, Long> {

    @Override
    public Long convertToDatabaseColumn(String s) {
        return Long.valueOf(s);
    }

    @Override
    public String convertToEntityAttribute(Long a) {
        try {
            Date date = DateUtils.parseDate(a.toString(),"yyyyMMddHHmmss");
            return DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss");
        } catch (Exception e){
            return "";
        }
    }
}
