package com.linln.admin.personMch.convert;

import javax.persistence.AttributeConverter;

public class LongConverter implements AttributeConverter<String, Long> {
    @Override
    public Long convertToDatabaseColumn(String s) {
        return Long.valueOf(s);
    }

    @Override
    public String convertToEntityAttribute(Long aLong) {
        if(aLong == null){
            aLong = 0L;
        }
        Double result = aLong/100d;
        return result.toString();
    }
}
