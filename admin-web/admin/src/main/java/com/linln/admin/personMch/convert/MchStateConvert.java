package com.linln.admin.personMch.convert;

import com.california.pay.consts.Status;

import javax.persistence.AttributeConverter;

/**
 * Created by thinkpad on 2019/1/4.
 */
public class MchStateConvert implements AttributeConverter<Status, String> {
    @Override
    public String convertToDatabaseColumn(Status status) {
        return status.name();
    }

    @Override
    public Status convertToEntityAttribute(String s) {
        return Status.valueOf(s);
    }
}
