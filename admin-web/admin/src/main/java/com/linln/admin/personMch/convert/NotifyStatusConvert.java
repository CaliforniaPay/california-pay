package com.linln.admin.personMch.convert;

import com.california.pay.consts.NotifyStatus;

import javax.persistence.AttributeConverter;

/**
 * Created by thinkpad on 2019/1/4.
 */
public class NotifyStatusConvert implements AttributeConverter<NotifyStatus, String> {
    @Override
    public String convertToDatabaseColumn(NotifyStatus notifyStatus) {
        return notifyStatus.name();
    }

    @Override
    public NotifyStatus convertToEntityAttribute(String s) {
        return NotifyStatus.valueOf(s);
    }
}
