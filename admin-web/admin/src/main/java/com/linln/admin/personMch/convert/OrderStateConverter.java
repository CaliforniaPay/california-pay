package com.linln.admin.personMch.convert;

import com.california.pay.consts.OrderStatus;

import javax.persistence.AttributeConverter;

/**
 * Created by thinkpad on 2019/1/4.
 */
public class OrderStateConverter  implements AttributeConverter<OrderStatus,String> {

    @Override
    public String convertToDatabaseColumn(OrderStatus orderStatus) {
        return orderStatus.name();
    }

    @Override
    public OrderStatus convertToEntityAttribute(String s) {
        return OrderStatus.valueOf(s);
    }
}
