package com.linln.admin.personMch.convert;

import com.linln.admin.personMch.consts.SettleStatus;

import javax.persistence.AttributeConverter;

/**
 * Created by thinkpad on 2019/1/4.
 */
public class SettleStatusConvert implements AttributeConverter<SettleStatus, String> {
    @Override
    public String convertToDatabaseColumn(SettleStatus status) {
        return status.name();
    }

    @Override
    public SettleStatus convertToEntityAttribute(String s) {
        return SettleStatus.valueOf(s);
    }
}
