package com.linln.admin.personMch.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by thinkpad on 2018/12/22.
 */
@Entity
@Table(name="p_mch_info")
@Data
@EntityListeners(AuditingEntityListener.class)
public class MchInfo implements Serializable {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String mchId;

    private String mchNo;

    private String mchName;

    private Long settleAmount;

    private Long balanceAmount;

    private Long totalAmount;

    private BigDecimal fee;

    private String type = "1";

    @NotEmpty(message = "请求私钥不能为空")
    private String reqKey;

    @NotEmpty(message = "响应私钥不能为空")
    private String resKey;

    private boolean state;
    //
    private String remark;
    // 创建时间
    @CreatedDate
    private Date createTime;
    // 更新时间
    @LastModifiedDate
    private Date updateTime;

    @Version
    private int version;
}
