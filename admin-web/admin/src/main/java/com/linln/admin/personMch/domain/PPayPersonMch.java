package com.linln.admin.personMch.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author 小懒虫
 * @date 2018/12/21
 */
@Entity
@Table(name="p_pay_person_mch")
@Data
@EntityListeners(AuditingEntityListener.class)
public class PPayPersonMch implements Serializable {
	// 主键ID
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String personMchId;
	// 渠道编码
	private String channelNo;
	// 渠道名称
	private String channelName;
	// 渠道个人账号
	private String channelMchAccount;
	// 渠道用户ID
	private String channelUserId;
	// 
	private String state;

	@Transient
	private String loginState;

	@Transient
	private Long todayAmount;
	// 
	private Long maxAmount;
	// 
	private String remark;
	// 创建时间
	@CreatedDate
	private Date createTime;
	// 更新时间
	@LastModifiedDate
	private Date updateTime;
}
