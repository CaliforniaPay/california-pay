package com.linln.admin.personMch.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.california.pay.consts.NotifyStatus;
import com.california.pay.consts.OrderStatus;
import com.linln.admin.personMch.convert.DateConverter;
import com.linln.admin.personMch.convert.LongConverter;
import com.linln.admin.personMch.convert.NotifyStatusConvert;
import com.linln.admin.personMch.convert.OrderStateConverter;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by thinkpad on 2018/12/22.
 */
@Entity
@Table(name="p_pay_order")
@Data
@EntityListeners(AuditingEntityListener.class)
public class PayOrder {
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String orderId;

    /**
     *内部商户订单号
     */
    @Excel(name = "内部商户订单号", orderNum = "0")
    private String innerOrderNo;

    /**
     *内部商户ID
     */
    @Excel(name = "商户编号", orderNum = "1")
    private String innerMchNo;

    /**
     *支付(平台)订单号
     */
    @Excel(name = "商户订单号", orderNum = "2")
    private String mchOrderNo;

    /**
     *请求总支付金额,单位分
     */
    @Convert(converter = LongConverter.class)
    @Excel(name = "支付金额", orderNum = "3")
    private String totalAmount;

    /**
     *实付金额
     */
    private Long txnAmount;

    /**
     *三位货币代码,人民币:cny
     */
    private String currency;

    /**
     *订单生成时间
     */
    @Convert(converter = DateConverter.class)
    @Excel(name = "订单生成时间",orderNum = "4")
    private String orderGenTime;

    /**
     *
     */
    @Convert(converter = DateConverter.class)
    @Excel(name = "订单过期时间",orderNum = "5")
    private String orderExpireTime;

    /**
     *订单支付成功时间,格式yyyyMMddhhmmss
     */
    @Convert(converter = DateConverter.class)
    @Excel(name = "订单支付时间",orderNum = "6")
    private String paySuccTime;

    /**
     *支付状态,0-订单生成,1-支付中(目前未使用),2-支付成功,3-订单完成(不可退款), 4-订单失败关闭
     */
    //@Enumerated(EnumType.STRING)
    @Convert(converter = OrderStateConverter.class)
    @Excel(name = "支付状态",enumExportField = "desc",orderNum = "7")
    private OrderStatus orderStatus;

    /**
     *客户端IP
     */
    @Excel(name = "客户端IP", orderNum = "8")
    private String clientIp;

    /**
     *设备
     */
    private String device;

    /**
     *终端设备
     */
    @Excel(name = "终端设备ID", orderNum = "9")
    private String terminalId;

    /**
     *商品标题
     */
    @Excel(name = "商品标题", orderNum = "10")
    private String goodsName;

    /**
     *商品描述信息
     */
    @Excel(name = "商品描述信息", orderNum = "11")
    private String goodsDesc;

    /**
     *特定渠道发起时额外参数
     */
    private String extra;

    /**
     *渠道商户ID
     */
    @Excel(name = "渠道商户ID", orderNum = "12")
    private String channelMchId;

    /**
     *渠道商户类型
     */
    private String channelMchType;

    /**
     *渠道appID
     */
    private String channelAppId;

    /**
     *渠道用户ID
     */
    @Excel(name = "支付宝用户ID", orderNum = "13")
    private String channelUserId;

    /**
     *渠道订单号
     */
    @Excel(name = "支付宝用户ID", orderNum = "14")
    private String channelOrderNo;

    /**
     *渠道支付错误码
     */
    private String channelErrCode;

    /**
     *渠道支付错误描述
     */
    private String channelErrMsg;

    /**
     *渠道最后通知时间, 格式yyyyMMddhhmmss
     */
    @Convert(converter = DateConverter.class)
    @Excel(name = "渠道最后通知时间", orderNum = "15")
    private String channelLastNotifyTime;

    /**
     *扩展参数1
     */
    private String param1;

    /**
     *扩展参数2
     */
    private String param2;

    /**
     *通知地址
     */
    private String mchNotifyUrl;

    /**
     *同步URL
     */
    private String mchReturnUrl;

    /**
     * 通知次数
     */
    @Excel(name = "渠道通知次数", orderNum = "16")
    private Integer mchNotifyCount;

    /**
     * SUCC, TIMEOUT, FAIL
     */
    @Convert(converter = NotifyStatusConvert.class)
    @Excel(name = "渠道通知状态", enumExportField = "desc",orderNum = "17")
    private NotifyStatus mchNotifyStatus;

    /**
     * 最后一次通知时间, 格式yyyyMMddhhmmss
     */
    @Convert(converter = DateConverter.class)
    private String lastMchNotifyTime;

    /**
     *
     */
    private String mchReturnStatus;

    /**
     * 备注
     */
    private String remark;

    /**
     *
     */
    // 创建时间
    @CreatedDate
    private Date createTime;

    /**
     *更新时间
     */
    // 更新时间
    @LastModifiedDate
    private Date updateTime;

    /**
     *支付时间，yyyyMMdd
     */
    private Long payDate;

    /**
     *支付结果触发，手工、系统回调
     */
    private String payResultTrigger;

}
