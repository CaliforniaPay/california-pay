package com.linln.admin.personMch.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.linln.admin.personMch.consts.AccountAttr;
import com.linln.admin.personMch.consts.AccountType;
import com.linln.admin.personMch.consts.SettleStatus;
import com.linln.admin.personMch.convert.AccountAttrConvert;
import com.linln.admin.personMch.convert.AccountTypeConvert;
import com.linln.admin.personMch.convert.SettleStatusConvert;
import com.linln.admin.system.domain.User;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 小懒虫
 * @date 2019/01/04
 */
@Entity
@Table(name="p_settle_account")
@Data
@EntityListeners(AuditingEntityListener.class)
public class SettleAccount implements Serializable {
	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;

	private String settleNo;
	// 结算日期
	private String settleDate;
	// 结算方式
	private String settleType;
	// 结算金额
	private Long settleAmount;
	// 打款金额
	private Long payAmount;
	// 手续费
	private BigDecimal fee;
	// 账户属性
	@Convert(converter = AccountAttrConvert.class)
	private AccountAttr accountAttr;
	// 账户类型
	@Convert(converter = AccountTypeConvert.class)
	private AccountType accountType;
	// 开户行名称
	private String accountBankName;
	// 开户行网点名称
	private String accountBankAddr;
	// 账号名
	private String accountName;	// 账户号
	private String accountNo;
	// 结算状态
	@Convert(converter = SettleStatusConvert.class)
	private SettleStatus settleStatus;
	// 备注
	private String remark;

	private String mchNo;

	// 创建时间
	@CreatedDate
	private Date createDate;
	// 更新时间
	@LastModifiedDate
	private Date updateDate;
	// 创建者
	@CreatedBy
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="create_by")
	@JsonIgnore
	private User createBy;
	// 更新者
	@LastModifiedBy
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="update_by")
	@JsonIgnore
	private User updateBy;

	private Long auditBy;

	private Date auditDate;

	private Long payBy;

	private Date payDate;
}
