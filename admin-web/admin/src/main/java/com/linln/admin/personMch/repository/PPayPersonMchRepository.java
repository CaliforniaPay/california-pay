package com.linln.admin.personMch.repository;

import com.linln.admin.personMch.domain.PPayPersonMch;
import com.linln.admin.system.repository.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author 小懒虫
 * @date 2018/12/21
 */
public interface PPayPersonMchRepository extends JpaRepository<PPayPersonMch, String>, JpaSpecificationExecutor<PPayPersonMch> {
    public PPayPersonMch findByPersonMchId(String id);

    public PPayPersonMch findByChannelUserId(String chanelUserId);

    @Modifying
    @Transactional
    @Query("delete from #{#entityName} es where es.personMchId = ?1")
    public int deleteByIds(List<String> id);

    /**
     * 获取登录id
     */
    @Query(value="SELECT login_id FROM p_terminal_login WHERE channel_user_id=?1", nativeQuery = true)
    public String getLoginIdBy(String chanelUserId);

    @Query(value="SELECT sum(pay_total_amount) FROM p_mch_pay_quota WHERE channel_mch_id=?2 and pay_date=?1", nativeQuery = true)
    public Long sumByDateAndChanelMchId(Long date, String chanelMchId);

    @Query(value="SELECT channel_user_id FROM p_terminal_login WHERE login_status='LOGIN'", nativeQuery = true)
    public List<Object> findAllLogin();

    @Query(value="SELECT channel_user_id FROM p_pay_person_mch WHERE channel_user_id not in  (SELECT channel_user_id FROM p_terminal_login WHERE login_status='LOGIN')", nativeQuery = true)
    public List<Object> findAllNotLogin();

}

