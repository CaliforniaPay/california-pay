package com.linln.admin.personMch.repository;

import com.california.pay.consts.OrderStatus;
import com.linln.admin.personMch.domain.PayOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Created by thinkpad on 2018/12/22.
 */
public interface PayOrderRepository extends JpaRepository<PayOrder, String>,JpaSpecificationExecutor<PayOrder> {
    @Query(value = "select sum(totalAmount) From PayOrder where orderStatus=?1 and innerMchNo=?2")
    public Long sumPayAmountByOrderStatus(OrderStatus orderStatus,String mchNo);

    @Query(value = "select sum(totalAmount) From PayOrder where orderStatus=?1 and innerMchNo=?2 and createTime>=?3 and createTime<=?4")
    public Long sumPayAmountByOrderStatus(OrderStatus orderStatus, String mchNo, Date begin, Date end);

}
