package com.linln.admin.personMch.repository;

import com.linln.admin.personMch.domain.SettleAccount;
import com.linln.admin.system.domain.User;
import com.linln.admin.system.repository.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;


/**
 * @author 小懒虫
 * @date 2019/01/04
 */
public interface SettleAccountRepository extends JpaRepository<SettleAccount, String>,JpaSpecificationExecutor<SettleAccount> {
    @Query(value = "select sum(settleAmount) From SettleAccount where settleStatus in ('PASS','DONE') and createBy=?1 and createDate>=?2 and createDate<=?3")
    public Long sumSettleAmountByOrderStatus(User user, Date begin, Date end);

    @Query(value = "select sum(settleAmount) From SettleAccount where settleStatus in ('PASS','DONE') and createBy=?1")
    public Long sumSettleAmountByUser(User user);
}

