package com.linln.admin.personMch.service;

import com.linln.admin.core.enums.StatusEnum;
import com.linln.admin.personMch.domain.PPayPersonMch;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2018/12/21
 */
public interface PPayPersonMchService {

    Page<PPayPersonMch> getPageList(PPayPersonMch pPayPersonMch, Integer pageIndex, Integer pageSize);

    PPayPersonMch getId(String id);

    PPayPersonMch save(PPayPersonMch pPayPersonMch);

    PPayPersonMch getByChannelUserId(String chanelUserId);

    int deleteByIds(List<String> ids);
}

