package com.linln.admin.personMch.service;

import com.california.pay.consts.OrderStatus;
import com.linln.admin.personMch.domain.PayOrder;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;

import java.util.Date;
import java.util.List;

/**
 * Created by thinkpad on 2018/12/22.
 */
public interface PayOrderService {
    Page<PayOrder> getPageList(PayOrder payOrder, Integer pageIndex, Integer pageSize);

    List<PayOrder> getList(PayOrder payOrder);

    PayOrder getById(String id);

    Long sumPayAmountByOrderStatus(OrderStatus orderStatus, String mchNo);

    Long sumPayAmountByOrderStatus(OrderStatus orderStatus, String mchNo, Date begin, Date end);
}
