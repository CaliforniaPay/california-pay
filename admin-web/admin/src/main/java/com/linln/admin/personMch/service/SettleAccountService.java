package com.linln.admin.personMch.service;

import com.linln.admin.personMch.domain.SettleAccount;
import com.linln.admin.system.domain.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author 小懒虫
 * @date 2019/01/04
 */
public interface SettleAccountService {

    Page<SettleAccount> getPageList(SettleAccount settleAccount, Integer pageIndex, Integer pageSize);

    SettleAccount getId(String id);

    SettleAccount save(SettleAccount settleAccount);

    Long sumSettleAmountByOrderStatus(User user, Date begin, Date end);

    Long sumSettleAmountByUser(User user);
}

