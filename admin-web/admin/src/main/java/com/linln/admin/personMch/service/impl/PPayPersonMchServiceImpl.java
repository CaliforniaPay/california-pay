package com.linln.admin.personMch.service.impl;

import com.linln.admin.core.enums.StatusEnum;
import com.linln.admin.core.web.QuerySpec;
import com.linln.admin.personMch.domain.PPayPersonMch;
import com.linln.admin.personMch.repository.PPayPersonMchRepository;
import com.linln.admin.personMch.service.PPayPersonMchService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author 小懒虫
 * @date 2018/12/21
 */
@Service
public class PPayPersonMchServiceImpl implements PPayPersonMchService {

    @Autowired
    private PPayPersonMchRepository pPayPersonMchRepository;

    /**
     * 根据商户个人渠道管理ID查询商户个人渠道管理数据
     * @param id 商户个人渠道管理ID
     */
    @Override
    @Transactional
    public PPayPersonMch getId(String id) {
        return pPayPersonMchRepository.findByPersonMchId(id);
    }

    @Override
    @Transactional
    public PPayPersonMch getByChannelUserId(String id) {
        return pPayPersonMchRepository.findByChannelUserId(id);
    }
    /**
     * 获取分页列表数据
     * @param pageIndex 页码
     * @param pageSize 获取列表长度
     * @return 返回分页数据
     */
    @Override
    public Page<PPayPersonMch> getPageList(PPayPersonMch pPayPersonMch, Integer pageIndex, Integer pageSize) {
        // 创建分页对象
        Sort sort = new Sort(Sort.Direction.DESC, "createTime");
        PageRequest page = PageRequest.of(pageIndex-1, pageSize, sort);
        QuerySpec querySpec = QuerySpec.matching()
                .withMatcher("channelMchAccount", QuerySpec.LIKE)
                .withMatcher("channelUserId", QuerySpec.LIKE).withIgnorePaths("createTime","loginState","remark");

        if(pPayPersonMch.getLoginState() !=null){
            if("1".equals(pPayPersonMch.getLoginState())){
                List<Object> logins = pPayPersonMchRepository.findAllLogin();
                if(logins.size() == 0){
                    pPayPersonMch.setChannelUserId("NOT_LOGIN");
                }else{
                    querySpec.withMatcherIn("channelUserId", logins);
                }
            }else{
                List<Object> notLogins = pPayPersonMchRepository.findAllNotLogin();
                querySpec.withMatcherIn("channelUserId", notLogins);
            }
        }

        Page<PPayPersonMch> list = pPayPersonMchRepository.findAll(QuerySpec.of(pPayPersonMch, querySpec), page);
        for (PPayPersonMch p: list.getContent()) {
            Long date = Long.valueOf(DateFormatUtils.format(new Date(), "yyyyMMdd"));
            Long todayAmount = pPayPersonMchRepository.sumByDateAndChanelMchId(date, p.getPersonMchId());
            todayAmount = todayAmount==null?0L:todayAmount;
            p.setTodayAmount(todayAmount);
            if (pPayPersonMch.getLoginState() != null){
                p.setLoginState(pPayPersonMch.getLoginState());
            }else{
                String loginId = pPayPersonMchRepository.getLoginIdBy(p.getChannelUserId());
                p.setLoginState("1");
                if(loginId == null){
                    p.setLoginState("2");
                }
            }
        }
        return list;
    }

    /**
     * 保存商户个人渠道管理
     * @param pPayPersonMch 商户个人渠道管理实体类
     */
    @Override
    public PPayPersonMch save(PPayPersonMch pPayPersonMch){
        return pPayPersonMchRepository.save(pPayPersonMch);
    }

    @Override
    public int deleteByIds(List<String> ids){
        return pPayPersonMchRepository.deleteByIds(ids);
    }
}

