package com.linln.admin.personMch.service.impl;

import com.california.pay.consts.OrderStatus;
import com.linln.admin.core.web.PageSort;
import com.linln.admin.core.web.QuerySpec;
import com.linln.admin.personMch.domain.PayOrder;
import com.linln.admin.personMch.repository.PayOrderRepository;
import com.linln.admin.personMch.service.PayOrderService;
import com.linln.core.utils.HttpServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by thinkpad on 2018/12/22.
 */
@Service
public class PayOrderServiceImpl implements PayOrderService{
    @Autowired
    private PayOrderRepository payOrderRepository;

    /**
     * 获取分页列表数据
     * @param payOrder 查询实例
     * @param pageIndex 页码
     * @param pageSize 获取列表长度
     * @return 返回分页数据
     */
    @Override
    public Page<PayOrder> getPageList(PayOrder payOrder, Integer pageIndex, Integer pageSize) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest("createTime","desc");
        QuerySpec querySpec = QuerySpec.matching()
                .withMatcher("innerOrderNo", QuerySpec.LIKE)
                .withMatcher("outOrderNo", QuerySpec.LIKE)
                .withMatcher("innerMchNo", QuerySpec.LIKE)
                .withMatcher("channelUserId", QuerySpec.LIKE)
                .withIgnorePaths("remark");
        String betweenDate = HttpServletUtil.getParameter("betweenDate");
        if(betweenDate != null){
            String[] date = betweenDate.replace("-","").split("~");
            try {
                payOrder.setCreateTime(new Date());
                querySpec = querySpec.withMatcherBetween("createTime"
                        ,Long.valueOf(date[0].trim()+"000000"),Long.valueOf(date[1].trim()+"235959"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Page<PayOrder> list = payOrderRepository.findAll(QuerySpec.of(payOrder, querySpec), page);
        return list;
    }

    @Override
    public List<PayOrder> getList(PayOrder payOrder){
        PageRequest page = PageSort.pageRequest("createTime","desc");
        QuerySpec querySpec = QuerySpec.matching()
                .withMatcher("innerOrderNo", QuerySpec.LIKE)
                .withMatcher("outOrderNo", QuerySpec.LIKE)
                .withMatcher("innerMchNo", QuerySpec.LIKE)
                .withIgnorePaths("remark");
        String betweenDate = HttpServletUtil.getParameter("betweenDate");
        if(betweenDate != null){
            String[] date = betweenDate.replace("-","").split("~");
            try {
                payOrder.setCreateTime(new Date());
                querySpec = querySpec.withMatcherBetween("createTime"
                        ,Long.valueOf(date[0].trim()+"000000"),Long.valueOf(date[1].trim()+"235959"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return payOrderRepository.findAll(QuerySpec.of(payOrder, querySpec), page.getSort());
    }
    @Override
    public PayOrder getById(String id) {
        return payOrderRepository.getOne(id);
    }

    @Override
    public Long sumPayAmountByOrderStatus(OrderStatus orderStatus,String mchNo){
        return payOrderRepository.sumPayAmountByOrderStatus(orderStatus, mchNo);
    }

    @Override
    public Long sumPayAmountByOrderStatus(OrderStatus orderStatus, String mchNo, Date begin, Date end){
        return payOrderRepository.sumPayAmountByOrderStatus(orderStatus, mchNo,begin,end);
    }
}
