package com.linln.admin.personMch.service.impl;

import com.linln.admin.core.enums.StatusEnum;
import com.linln.admin.core.web.PageSort;
import com.linln.admin.core.web.QuerySpec;
import com.linln.admin.personMch.domain.SettleAccount;
import com.linln.admin.personMch.repository.SettleAccountRepository;
import com.linln.admin.personMch.service.SettleAccountService;
import com.linln.admin.system.domain.User;
import com.linln.core.utils.HttpServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author 小懒虫
 * @date 2019/01/04
 */
@Service
public class SettleAccountServiceImpl implements SettleAccountService {

    @Autowired
    private SettleAccountRepository settleAccountRepository;

    /**
     * 根据结算订单ID查询结算订单数据
     * @param id 结算订单ID
     */
    @Override
    @Transactional
    public SettleAccount getId(String id) {
        return settleAccountRepository.getOne(id);
    }

    /**
     * 获取分页列表数据
     * @param settleAccount 查询实例
     * @param pageIndex 页码
     * @param pageSize 获取列表长度
     * @return 返回分页数据
     */
    @Override
    public Page<SettleAccount> getPageList(SettleAccount settleAccount, Integer pageIndex, Integer pageSize) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest("createDate","desc");
        QuerySpec querySpec = QuerySpec.matching()
                .withMatcher("settleNo", QuerySpec.LIKE)
                .withMatcher("accountName", QuerySpec.LIKE)
                .withMatcher("accountNo", QuerySpec.LIKE)
                .withIgnorePaths("remark");
        String betweenDate = HttpServletUtil.getParameter("betweenDate");
        if(betweenDate != null){
            String[] date = betweenDate.replace("-","").split("~");
            try {
                settleAccount.setCreateDate(new Date());
                querySpec = querySpec.withMatcherBetween("createDate"
                        ,Long.valueOf(date[0].trim()+"000000"),Long.valueOf(date[1].trim()+"235959"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Page<SettleAccount> list = settleAccountRepository.findAll(QuerySpec.of(settleAccount, querySpec), page);
        return list;
    }

    /**
     * 保存结算订单
     * @param settleAccount 结算订单实体类
     */
    @Override
    public SettleAccount save(SettleAccount settleAccount){
        return settleAccountRepository.save(settleAccount);
    }

    @Override
    public Long sumSettleAmountByOrderStatus(User user, Date begin, Date end){
        return  settleAccountRepository.sumSettleAmountByOrderStatus(user, begin,end);
    }

    public Long sumSettleAmountByUser(User user){
        return  settleAccountRepository.sumSettleAmountByUser(user);
    }
}

