package com.linln.admin.personMch.validator;

import com.linln.admin.personMch.domain.PPayPersonMch;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * @author 小懒虫
 * @date 2018/12/21
 */
@Data
public class PPayPersonMchForm extends PPayPersonMch implements Serializable {
	@NotEmpty(message = "渠道编码不能为空")
	private String channelNo;
	@NotEmpty(message = "渠道名称不能为空")
	private String channelName;
	@NotEmpty(message = "渠道个人账号不能为空")
	private String channelMchAccount;
	@NotEmpty(message = "渠道用户ID不能为空")
	private String channelUserId;
	@Digits(integer = 12,fraction = 2,message = "不是数字")
	@NotNull(message = "不能为空")
	private Long maxAmount;
}
