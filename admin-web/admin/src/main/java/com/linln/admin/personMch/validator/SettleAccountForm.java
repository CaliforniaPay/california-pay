package com.linln.admin.personMch.validator;

import com.linln.admin.personMch.domain.SettleAccount;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * @author 小懒虫
 * @date 2019/01/04
 */
@Data
public class SettleAccountForm extends SettleAccount implements Serializable {

	@Digits(integer = 12,fraction = 2,message = "结算金额不是数字")
	private Long settleAmount;
	@Digits(integer = 12,fraction = 2,message = "打款金额不是数字")
	private Long payAmount;
}
