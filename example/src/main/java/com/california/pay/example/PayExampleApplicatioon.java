package com.california.pay.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: california pay demo 启动类
 * @author: KeyboardMan
 * @Date: Create in 2018-12-31:13:24
 */
@SpringBootApplication
public class PayExampleApplicatioon {
    public static void main(String[] args) {
        SpringApplication.run(PayExampleApplicatioon.class, args);
    }




}
