package com.california.pay.example.controller;


import com.california.pay.example.entity.PayEntity;
import com.california.pay.example.service.PayService;
import com.california.pay.example.utils.MD5Sign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Description: 支付示例controller
 * @author: KeyboardMan
 * @Date: Create in 2018-12-31:13:31
 */
@Slf4j
@RequestMapping("/pay")
@RestController
public class PayExampleController {


    @Autowired
    PayService payService;


    /**
     * 请码
     * @return
     */
    @GetMapping(value="/execute",
                 produces = "text/html")
    public String pay() {
        return payService.getPay();
    }


    /**
     * 收 异步回调通知接口
     * @param reqMap
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/notify",method = RequestMethod.POST)
    @ResponseBody
    public String AsyncNotify(@RequestBody Map<String,Object> reqMap) throws Exception {
        log.info(reqMap.toString());
        String resSecret = ""; // 改成自己的res_key
        boolean result = MD5Sign.verify(reqMap,resSecret);
        log.info("验签结果 {}",result);
        return reqMap.get("innerOrderNo").toString();
    }
}
