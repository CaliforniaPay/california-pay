package com.california.pay.example.entity;


/**
 * @Description:
 * @author: KeyboardMan
 * @Date:
 */
public class PayEntity {

    // 商户编号
    private String innerMchNo;

    // 订单编号
    private String outOrderNo;

    // 金额
    private Long amount;

    // 支付类型
    private String payType;

    // 商品名称
    private String goodsName;

    // 商品描述
    private String goodsDesc;

    // IP地址
    private String clientIp;

    // 异步通知URL
    private String notifyUrl;

    // 同步通知URL
    private String returnUrl;

    // 订单备注
    private String remark;

    // MD5 签名串
    private String sign;


    public String getInnerMchNo() {
        return innerMchNo;
    }

    public void setInnerMchNo(String innerMchNo) {
        this.innerMchNo = innerMchNo;
    }

    public String getOutOrderNo() {
        return outOrderNo;
    }

    public void setOutOrderNo(String outOrderNo) {
        this.outOrderNo = outOrderNo;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
