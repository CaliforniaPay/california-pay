package com.california.pay.example.utils;


import com.california.pay.example.entity.PayEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date:
 */

public class HttpUtil {

    private HttpUtil(){}


    public static String post(){
        String url = "";
        HttpPost post = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();

            post = new HttpPost("http://185.239.226.39:10080/gateway/getQR");
            // 构造消息头
            post.setHeader("Content-type", "application/json; charset=utf-8");

            // 构建消息实体
            StringEntity entity = new StringEntity(getEntity(), Charset.forName("UTF-8"));
            entity.setContentEncoding("UTF-8");
            // 发送Json格式的数据请求
            entity.setContentType("application/json");
            post.setEntity(entity);

            HttpResponse response = httpClient.execute(post);

            return EntityUtils.toString(response.getEntity(), "utf-8");

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(post != null){
                try {
                    post.releaseConnection();
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return url;
    }


    /**
     * 请替换成自己的参数
     * @return
     */
    public static String getEntity() {

        PayEntity payOrderRequest = new PayEntity();
        payOrderRequest.setInnerMchNo("P200");
        payOrderRequest.setAmount(200000L);
        payOrderRequest.setClientIp("192.168.0.32");
        payOrderRequest.setOutOrderNo(System.currentTimeMillis()+"");
        payOrderRequest.setRemark("这是订单备注信息");
        payOrderRequest.setNotifyUrl("http://185.239.226.39:10080/test/notify");
        payOrderRequest.setGoodsName("冬天暖手袋");
        payOrderRequest.setGoodsDesc("限购一件");
        payOrderRequest.setReturnUrl("http://185.239.226.39:10080/test/return?aa=ss&ww=dd");
        payOrderRequest.setPayType("alipay5");

        try {
            Map<String, Object> map = BeanUtil.obj2Map(payOrderRequest);
            payOrderRequest.setSign(MD5Sign.sign(map,"1"));
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return JsonUtil.obj2String(payOrderRequest);
    }
}