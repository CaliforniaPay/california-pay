package com.california.pay.example.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

/**
 * @Description: MD5 签名工具类
 * @author: KeyboardMan
 * @Date:
 */
public class MD5Sign {

    private MD5Sign(){}

    /**
     * 方法描述:将字符串MD5加码 生成32位md5码
     * @param inStr
     * @return
     */
    public static String md5(String inStr) {
        try {
            return DigestUtils.md5Hex(inStr.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误");
        }
    }

    /**
     * 方法描述:签名字符串
     * @param params 需要签名的参数
     * @param appSecret 签名密钥
     * @return
     */
    public static String sign(Map<String, Object> params, String appSecret) {
        StringBuilder valueSb = new StringBuilder();
        params.put("secretKey", appSecret);
        Map<String, Object> sortParams = new TreeMap<>(params);
        Set<Entry<String, Object>> entrys = sortParams.entrySet();
        for (Entry<String, Object> entry : entrys) {
            valueSb.append(entry.getValue());
        }
        params.remove("secretKey");
        return md5(valueSb.toString());
    }

    /**
     * 方法描述:验证签名
     * @param secretKey 加密秘钥
     * @param params 参数
     * @return
     * @throws Exception
     */
    public static boolean verify(Map<String, Object> params, String secretKey) throws Exception {
        String sign = params.get("sign").toString();
        params.remove("sign");
        params.put("secretKey", secretKey);
        Map<String, Object> sortParams = new TreeMap<>(params);
        Set<Entry<String, Object>> entrys = sortParams.entrySet();
        StringBuilder valueSb = new StringBuilder();
        for (Entry<String, Object> entry : entrys) {
            valueSb.append(entry.getValue());
        }
        String mysign = md5(valueSb.toString());
        if (mysign.equals(sign)) {
            return true;
        } else {
            return false;
        }
    }
}