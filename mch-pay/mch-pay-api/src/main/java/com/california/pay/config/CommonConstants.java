package com.california.pay.config;

/**
 * 常量类定义
 *
 * @author liangd.chen
 * @date 2018/10/31
 */
public class CommonConstants {

    /**
     * 代表字段值为null
     */
    public static final String COLUMN_NULL = "NULL";

    public static final String SYSTEM_CREATOR = "system";

    public static final String TAIL_END = "\r\n";
}
