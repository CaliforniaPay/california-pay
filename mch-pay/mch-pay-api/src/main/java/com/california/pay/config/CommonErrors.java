package com.california.pay.config;

/**
 * 通用错误
 */
public enum CommonErrors implements Err {

/*
00	指令执行成功
01	会话sessionId错误
02	无该客户端ID
03	验签错误
04	命令数据格式错
05	系统暂不支持命令
06	其他错误
07	客户端未注册
12	错误的APPID
14	请求参数为空或长度错误
15	请求后端服务出错
16	消息保存时出错
99	未知错误*/

    /**
     * 处理成功
     */
    SUCCESS("00", "指令执行成功"),

    SESSION_ERROR("01", "会话sessionId错误"),

    /**
     * 无该客户端ID
     */
    INVALID_APPID("02", "无该客户端ID"),
    /**
     * 签名验证失败
     */
    SIGNATURE_VERIFY_FAIL("03", "签名验证失败"),
    /**
     * 参数错误
     */
    INVALID_PARAM("04", "参数错误"),
    /**
     * 系统暂不支持命令
     */
    COMMAND_UNSUPPORT("05", "系统暂不支持命令"),

    /**
     * 客户端未登陆
     */
    CLIENT_UN_LOGIN("06", "客户端未登陆"),

    /**
     * 渠道端未注册
     */
    CLIENT_UN_REGISTER("07", "客户端未注册"),
    /**
     * 未找到匹配订单号
     */
    NOT_FUND_ORDER_NO("09", "未找到匹配订单号"),


    /**
     * 未知错误
     */
    UNDEFINED("9999", "未知错误");


    public static CommonErrors codeOf(String code) {
        for (CommonErrors error : CommonErrors.values()) {
            if (error.code.equals(code)) {
                return error;
            }
        }
        throw new IllegalArgumentException("No enum constant com.bwton.core.web.CommonErrors." + code);
    }

    /*************************************************************************************/
    /**
     * 编码
     */
    private String code;
    /**
     * 消息
     */
    private String msg;

    /**
     * 构造函数
     *
     * @param code
     * @param msg
     */
    CommonErrors(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }

    @Override
    public String toString() {
        return "Errors{" +
            "code='" + code + '\'' +
            ", msg='" + msg + '\'' +
            "} " + super.toString();
    }
}
