package com.california.pay.config;

import java.io.Serializable;

/**
 * @author liangd.chen
 * @date 18/6/14
 */
public class CommonMsgCode implements Err, Serializable {
    private static final long serialVersionUID = 3709495691183222154L;
    /**
     * 编码
     */
    private String code;
    /**
     * 消息
     */
    private String msg;

    /**
     * 构造函数
     *
     * @param code
     * @param msg
     */
    public CommonMsgCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }

    @Override
    public String toString() {
        return "Errors{" +
            "code='" + code + '\'' +
            ", msg='" + msg + '\'' +
            "} " + super.toString();
    }
}
