package com.california.pay.config;

/**
 * 错误
 *
 * @author Saintcy
 */
public interface Err {
    /**
     * 获取错误编码
     *
     * @return
     */
    String getCode();

    /**
     * 获取错误信息
     *
     * @return
     */
    String getMsg();
}
