package com.california.pay.consts;

import com.california.pay.config.Err;

/**
 * 渠道商户类型
 */

public enum ChannelMchType implements Err {
    /**
     *
     */
    ENT_MCH("00", "企业商户"),
    PERSON_MCH("01", "个人商户"),
    ;

    public String code;

    public String desc;

    ChannelMchType(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }
}
