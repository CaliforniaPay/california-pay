package com.california.pay.consts;

import com.california.pay.config.Err;


public enum ChannelType implements Err {
    /**
     * 支付宝个人转账
     */
    ALIPAY_PERSON("alipay.person", "支付宝个人转账"),

    ;

    public String code;

    public String desc;

    ChannelType(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }
}