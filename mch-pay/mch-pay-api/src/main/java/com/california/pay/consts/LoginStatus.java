package com.california.pay.consts;

import com.california.pay.config.Err;


public enum  LoginStatus implements Err {
    /**
     *
     */
    NONE("", "无"),
    LOGIN("00", "登录"),
    LOGOUT("01", "登出"),
    ;

    public String code;

    public String desc;

    LoginStatus(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }
}