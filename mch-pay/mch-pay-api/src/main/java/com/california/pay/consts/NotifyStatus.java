package com.california.pay.consts;

import com.california.pay.config.Err;

/**
 * SUCC, TIMEOUT, FAIL
 */
public enum NotifyStatus implements Err {
    NONE("", "无"),
    NOTIFY_SUCC("00", "成功"),
    NOTIFY_TIMEOUT("01", "超时"),
    NOTIFY_FAIL("02", "失败"),
    ;

    public String code;

    public String desc;

    NotifyStatus(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getDesc(){
        return this.desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }
}
