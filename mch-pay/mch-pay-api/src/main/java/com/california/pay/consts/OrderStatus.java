package com.california.pay.consts;

import com.california.pay.config.Err;

/**
 * SUCC, TIMEOUT, FAIL
 */
public enum OrderStatus implements Err {
    /**
     * 支付状态,0-订单生成,1-支付中(目前未使用),2-支付成功,3-订单完成(不可退款), 4-订单失败关闭
     */
    ORDER_GEN("00", "订单生成"),
    //PAYING("01", "支付中"),
    PAY_SUCC("02", "支付成功"),
    ORDER_FINISHED("03", "订单完成(不可退款)"),
    ORDER_FAILED("04", "订单失败"),
    ;

    public String code;

    public String desc;

    OrderStatus(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }}
