package com.california.pay.consts;

import com.california.pay.config.Err;

/**
 */
public enum PayResultTrigger implements Err {
    /**
     *
     */
    SYSTEM("00", "系统触发"),
    CSS("01", "手工触发补单"),
    ;

    public String code;

    public String desc;

    PayResultTrigger(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }
}
