package com.california.pay.consts;

import com.california.pay.config.Err;

/**
 */
public enum  Status implements Err {
    USABLE("00", "可用"),
    DISABLE("01", "不可用"),
    ;

    public String code;

    public String desc;

    Status(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }
}
