package com.california.pay.consts;

import com.california.pay.config.Err;

/**
 */
public enum YES_NOT implements Err {
    /**
     * 支付宝个人转账
     */
    YES("00", "是"),
    NOT("01", "否"),
    ;

    public String code;

    public String desc;

    YES_NOT(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMsg() {
        return null;
    }
}