package com.california.pay.facade;

import com.california.pay.model.MchOrderPayNotifyMchReq;
import com.california.pay.model.MchOrderPayNotifyMchRsp;
import com.california.pay.model.MchOrderPayNotifyReq;
import com.california.pay.model.MchOrderPayNotifyRsp;
import com.california.pay.result.CommonResult;

/**
 * 商户订单结果通知
 */
public interface MchOrderNotifyFacade {

    /**
     * 支付结果同步
     * 交易平台->open前置->商户平台
     *
     * @param notifyReq
     * @return
     */
    public CommonResult<MchOrderPayNotifyRsp> payOrderNotify(MchOrderPayNotifyReq notifyReq);

    /**
     * 支付结果通知
     * 交易平台->open前置->商户平台
     *
     * @param notifyMchReq
     * @return
     */
    public CommonResult<MchOrderPayNotifyMchRsp> payOrderNotifyMch(MchOrderPayNotifyMchReq notifyMchReq);


}
