package com.california.pay.facade;

import com.california.pay.model.OrderPayNotifyResultReq;
import com.california.pay.model.OrderPayNotifyResultRsp;
import com.california.pay.model.PersonOrderPayReq;
import com.california.pay.model.PersonOrderPayRsp;
import com.california.pay.result.CommonResult;

/**
 * 商户渠道订单接口
 */
public interface MchOrderPayFacade {

    /**
     * 个人商户请求支付，1.生成内部订单号；2.渠道账户路由; 3.终端验证
     * 流程: 商户平台->open 前置 -> 交易平台
     *
     * @param orderPayReq
     * @return
     */
    public CommonResult<PersonOrderPayRsp> personMchOrderPay(PersonOrderPayReq orderPayReq);

    /**
     * 结果通知反馈
     * 流程: 商户平台->open 前置 -> 交易平台
     * @return
     */
    public CommonResult<OrderPayNotifyResultRsp> orderpayNotifyAck(OrderPayNotifyResultReq notifyResultReq);

    /**
     * 补单：
     * 1. 通知外部商户补单；
     * 2. 修改订单状态，通知外部商户补单；
     * @param payOrderId
     * @return
     */
    public CommonResult<?> supplyPayOrder(String payOrderId);
}
