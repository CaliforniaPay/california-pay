package com.california.pay.facade;


import com.california.pay.result.CommonResult;

public interface TerminalManagerFacade {

    /**
     * 终端下线
     * @param channelUserId 支付宝userId
     * @return
     */
    public CommonResult<?> terminalLogout(String channelUserId);
}
