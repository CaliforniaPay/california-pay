package com.california.pay.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 商户订单支付结果通知信息
 */
@Data
public class MchOrderPayNotifyMchReq implements Serializable{
    private static final long serialVersionUID = 6564005121218261722L;

    /**
     * 外部订单号
     */
    private String outOrderNo;

    /**
     * 内部订单号
     */
    private String innerOrderNo;
    /**
     * 内部商户号
     */
    private String innerMchNo;
    /**
     * 请求总金额
     */
    private Long totalAmount;
    /**
     * 实付金额
     */
    private Long txnAmount;
    /**
     * 渠道用户ID
     */
    private String channelUserId;
    /**
     * 通知URL
     */
    private String notifyUrl;
    /**
     * 同步通知URL
     */
    private String returnUrl;
    /**
     * 回传备注
     */
    private String remark;
    /**
     * 当前系统事件
     */
    private LocalDateTime systemTime;
}
