package com.california.pay.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 通知
 */
@Data
public class MchOrderPayNotifyMchRsp implements Serializable{
    private static final long serialVersionUID = -5181561812594715124L;
    private LocalDateTime systemTime;

}
