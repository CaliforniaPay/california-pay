package com.california.pay.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 通知
 */
@Data
public class MchOrderPayNotifyRsp implements Serializable{
    private static final long serialVersionUID = -6293745927968760587L;
    private LocalDateTime systemTime;

}
