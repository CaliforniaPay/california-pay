package com.california.pay.model;

import com.california.pay.consts.NotifyStatus;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 应答结果
 */
@Data
public class OrderPayNotifyResultReq implements Serializable {
    /**
     * 外部订单号
     */
    private String outOrderNo;
    /**
     * 内部订单号
     */
    @NotBlank(message= "内部订单号不为空")
    private String innerOrderNo;
    /**
     * 渠道用户ID
     */
    private String channelUserId;
    /**
     * 回传备注
     */
    private String remark;
    /**
     * 系统时间
     */
    private LocalDateTime systemTime;

    /**
     * 调用商户成功或失败结果
     */
    NotifyStatus notifyStatus;
}
