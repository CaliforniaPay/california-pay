package com.california.pay.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 是否通知到应答
 */
@Data
public class OrderPayNotifyResultRsp implements Serializable{

    private static final long serialVersionUID = -2612893625617986896L;
    /**
     * 系统事件
     */
    private LocalDateTime systemTime;

}
