package com.california.pay.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 商户订单支付请求
 */
@Data
public class PersonOrderPayReq implements Serializable{
    private static final long serialVersionUID = 3419673502366176476L;
    /**
     * 内部商户号
     */
    @NotBlank(message = "内部商户号不为空")
    private String innerMchNo;
    /**
     * 请求金额
     */
    @NotNull(message = "请求金额不为空")
     private Long amount;
    /**
     * 外部订单号
     */
    @NotBlank(message = "商户订单号不为空")
    private String outOrderNo;

    /**
     * 固定填写 alipayh5  或 weixinh5
     */
    private String payType;
    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不为空")
    private String goodsName;
    /**
     * 商品描述
     */
    @NotBlank(message = "商品描述不为空")
    private String goodsDesc;
    /**
     * 用户IP地址
     */
    private String clientIp;
    /**
     * 同步URL
     */
    @NotBlank(message = "return_url不为空")
    private String returnUrl;
    /**
     * 回调URL
     */
    @NotBlank(message = "notify_url不为空")
    private String notifyUrl;
    /**
     * 备注
     */
    private String remark;
    /**
     * 当前系统时间
     */
    @NotNull(message = "请求当前系统时间不为空")
    private LocalDateTime systemTime;


}
