package com.california.pay.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 应答
 */
@Data
public class PersonOrderPayRsp implements Serializable {
    private static final long serialVersionUID = 2107344189395835677L;
    /**
     * 交易金额
     */
    private Long amount;

    /**
     * 外部订单号
     */
    private String outOrderNo;
    /**
     * 内部订单号
     */
    private String innerOrderNo;

    /**
     * 支付宝UserId
     */
    private String channelUserId;

    /**
     * 当前系统时间
     */
    private LocalDateTime systemTime;

}
