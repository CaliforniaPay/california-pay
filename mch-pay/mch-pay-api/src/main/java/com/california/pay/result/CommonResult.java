package com.california.pay.result;


import com.california.pay.config.CommonErrors;
import com.california.pay.config.Err;
import com.california.pay.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * 通用结果
 *
 * @author Saintcy
 */
public class CommonResult<T> extends Result<T> {
    public CommonResult(boolean success, String errcode, String errmsg) {
        super(success, errcode, errmsg);
    }

    public CommonResult(boolean success, String errcode, String errmsg, T result) {
        super(success, errcode, errmsg, result);
    }

    public static final <T> CommonResult<T> failure(String errcode, String errmsg) {
        return new CommonResult(CommonErrors.SUCCESS.getCode().equals(errcode), errcode, errmsg);
    }

    public static final <T> CommonResult<T> failure(String errmsg) {
        return new CommonResult(false, CommonErrors.INVALID_PARAM.getCode(), errmsg);
    }

    public static final <T> CommonResult<T> failure(Err err) {
        return new CommonResult(CommonErrors.SUCCESS == err, err.getCode(), err.getMsg());
    }

    public static final <T> CommonResult<T> failure(Throwable t) {
        return failure(null, t);
    }

    public static final <T> CommonResult<T> failure(String message, Throwable t) {
        String errcode = CommonErrors.UNDEFINED.getCode();
        String errmsg = StringUtils.isEmpty(message) ? (StringUtils.isEmpty(t.getMessage()) ? t.toString() : t.getMessage()) : "";
        if (t instanceof ConstraintViolationException) {
            if (StringUtils.isEmpty(message)) {
                StringBuffer sb = new StringBuffer();
                Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) t).getConstraintViolations();
                for (ConstraintViolation constraintViolation : constraintViolations) {
                    sb.append(StringUtils.isEmpty(constraintViolation.getMessage()) ? constraintViolation.toString() : constraintViolation.getMessage()).append("\r\n");
                }
                errmsg = CommonErrors.INVALID_PARAM.getMsg() + ": " + sb.toString();
            }
            errcode = CommonErrors.INVALID_PARAM.getCode();
        } else if (t instanceof IllegalArgumentException) {
            errcode = CommonErrors.INVALID_PARAM.getCode();
            errmsg = StringUtils.isEmpty(message) ? t.getMessage() : message;
        } else if (t instanceof BusinessException) {
            errcode = ((BusinessException) t).getCode();
            errmsg = StringUtils.isEmpty(message) ? t.getMessage() : message;
        }

        return failure(errcode, errmsg);
    }

    public static final <T> CommonResult<T> success() {
        return failure(CommonErrors.SUCCESS);
    }

    public static final <T> CommonResult<T> success(T data) {
        return new CommonResult(true, CommonErrors.SUCCESS.getCode(), CommonErrors.SUCCESS.getMsg(), data);
    }
}
