package com.california.pay.result;

import java.io.Serializable;

/**
 * 结果
 *
 * @author Saintcy
 */
public class Result<T> implements Serializable {
    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 错误代码
     */
    private String errcode;
    /**
     * 错误说明
     */
    private String errmsg;
    /**
     * 结果数据
     */
    private T result;

    public Result(boolean success, String errcode, String errmsg) {
        this.success = success;
        this.errcode = errcode;
        this.errmsg = errmsg;
    }

    public Result(boolean success, String errcode, String errmsg, T result) {
        this.success = success;
        this.errcode = errcode;
        this.errmsg = errmsg;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrcode() {
        return errcode;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Result{" +
            "errcode='" + errcode + '\'' +
            ", errmsg='" + errmsg + '\'' +
            ", result=" + result +
            '}';
    }
}
