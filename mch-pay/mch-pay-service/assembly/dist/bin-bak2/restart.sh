#!/bin/bash

sh ./stop.sh skip
sh ./start.sh

sleep 3

COUNT=0
STARTED=''
ERROR=''
while [ $COUNT -lt 120 ]; do
    echo -e ".\c"
    sleep 1
    COUNT=$[$COUNT+1]

    STARTED=`grep "Dubbo service server started\!" ../logs/stdout.log`
    ERROR=`grep " ERROR " ../logs/stdout.log`
    if [ -n "$STARTED" ] || [ -n "$ERROR" ]; then
        #COUNT=0
        break
    fi
done
echo ""
if [ -n "$STARTED" ]; then
  echo "Dubbo service server started!"
elif [ -n "$ERROR" ]; then
  echo "Start failed."
else
  echo "Start timeout."
fi
