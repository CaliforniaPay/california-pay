#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:    restart.sh
# Revision:    1.0
# Date:        2016/12/29
# Author:      chen liangdeng
# Email:       87961327@QQ.COM
# Description: 
# Notes: 
# -------------------------------------------------------------------------------
cd `dirname $0`
sh stop.sh

ret=$?
echo $ret
if [ $ret -ne 0 ]; then
    echo "ERROR: The Service stop failed, deploy exist!"
    exit 1
fi

sh start.sh
