#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:    service.sh
# Revision:    1.0
# Date:        2018/12/29
# Author:      chen liangdeng
# Email:       87961327@QQ.COM
# Description: 
# Notes:
# -------------------------------------------------------------------------------
cd `dirname $0`
BIN_DIR=`pwd`
cd ..
DEPLOY_DIR=`pwd`

start() {
    sh $BIN_DIR/start.sh
}

stop() {
     sh $BIN_DIR/stop.sh
}

## Check to see if we are running as root first.
## Found at http://www.cyberciti.biz/tips/shell-root-user-check-script.html  #
#if [ "$(id -u)" != "0" ]; then
#    echo "This script must be run as root" 1>&2
#    exit 1
#fi

case "$1" in
    start)
        start
        exit 0
    ;;
    stop)
        stop
        exit 0
    ;;
    reload|restart|force-reload)
        stop
        start
        exit 0
    ;;
    **)
        echo "Usage: $0 {start|stop|reload}" 1>&2
        exit 1
    ;;
esac