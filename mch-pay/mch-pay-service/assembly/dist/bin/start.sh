#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:    start.sh
# Revision:    1.0
# Description: 启动进程
# Notes:
# -------------------------------------------------------------------------------
# 需要替换main类名
JAVA_MAIN=com.california.pay.launcher.ChannelPayLauncher

cd `dirname $0`
BIN_DIR=`pwd`
cd ..

DEPLOY_DIR=`pwd`
CONF_DIR=$DEPLOY_DIR/config
LOGS_DIR=""

#java opts
JAVA_OPTS=""

if [ -n "$LOGS_FILE" ]; then
    LOGS_DIR=`dirname $LOGS_FILE`
else
    LOGS_DIR=$DEPLOY_DIR/logs
fi
if [ ! -d $LOGS_DIR ]; then
    mkdir $LOGS_DIR
fi
STDOUT_FILE=$LOGS_DIR/stdout.log

#setting log4j log path
JAVA_OPTS=$JAVA_OPTS" "-Dlog.path=$LOGS_DIR

JAVA_GC_OPTS = ""

SPRING_CONFIG = " -Ddubbo.spring.config=classpath*:spring.xml "

#拼接java参数
#while [ "$#" -ge 1 ];do
#    JAVA_OPTS=$JAVA_OPTS" "-D$1" "
#    let count=count+1
#    shift
#done

echo "JAVA_OPTS: "$JAVA_OPTS

#SERVER_PORT=`sed '/zk.dubbo.port/!d;s/.*=//' config/config.properties | tr -d '\r'`
PIDS=`ps -f | grep java | grep "$CONF_DIR" |awk '{print $2}'`

if [ -n "$PIDS" ]; then
    echo "ERROR: The Service already started!"
    echo "PID: $PIDS"
    exit 1
fi

#if [ -n "$SERVER_PORT" ]; then
#    SERVER_PORT_COUNT=`netstat -tln | grep $SERVER_PORT | wc -l`
#    if [ $SERVER_PORT_COUNT -gt 0 ]; then
#        echo "ERROR: The $SERVER_NAME port $SERVER_PORT already used!"
#        exit 1
#    fi
#fi

LIB_DIR=$DEPLOY_DIR/lib
# LIB_JARS=`ls $LIB_DIR|grep .jar|awk '{print "'$LIB_DIR'/"$0}'|tr "\n" ":"`
LIB_JARS=$LIB_DIR/*

JAVA_DEBUG_OPTS=""
if [ "$1" = "debug" ]; then
    JAVA_DEBUG_OPTS=" -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n "
fi
JAVA_JMX_OPTS=""
if [ "$1" = "jmx" ]; then
    JAVA_JMX_OPTS=" -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false "
fi
JAVA_MEM_OPTS=""
BITS=`java -version 2>&1 | grep -i 64-bit`
if [ -n "$BITS" ]; then
    #-Xmx512g -Xms512g -Xmn256m
    JAVA_MEM_OPTS=" -server -Xmx512m -Xms512m -Xmn256m -XX:PermSize=128m -Xss256k -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:LargePageSizeInBytes=128m -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 "
else
    JAVA_MEM_OPTS=" -server -Xms512m -Xmx512m -XX:PermSize=256m -XX:SurvivorRatio=2 -XX:+UseParallelGC "
fi

#JAVA_GC_OPTS=" -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=$LOGS_DIR/JVM/OOM.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:$LOGS_DIR/JVM/gc.log"


cd $BIN_DIR
nohup java $JAVA_OPTS $JAVA_MEM_OPTS $JAVA_GC_OPTS $JAVA_DEBUG_OPTS $JAVA_JMX_OPTS -Ddubbo.spring.config=classpath*:spring.xml -classpath $CONF_DIR:$LIB_JARS  $JAVA_MAIN > $STDOUT_FILE 2>&1 &


COUNT=0
while [ $COUNT -lt 1 ]; do
    echo -e ".\c"
    sleep 1
    #if [ -n "$SERVER_PORT" ]; then
    #  COUNT=`netstat -an | grep $SERVER_PORT | wc -l`
    #else
    #	COUNT=`ps -f | grep java | grep "$DEPLOY_DIR" | awk '{print $2}' | wc -l`
    #fi
    COUNT=`ps -f | grep java | grep "$DEPLOY_DIR" | awk '{print $2}' | wc -l`
    if [ $COUNT -gt 0 ]; then
        break
    fi
done

echo "Service start OK!"
PIDS=`ps -f | grep java | grep "$DEPLOY_DIR" | awk '{print $2}'`
echo "PID: $PIDS"
echo $PIDS > $BIN_DIR/pid
echo "STDOUT: $STDOUT_FILE"
