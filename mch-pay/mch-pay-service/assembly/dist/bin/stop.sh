#!/bin/bash
# -------------------------------------------------------------------------------
# Filename:    stop.sh
# Revision:    1.0
# Date:        2016/12/29
# Author:      chen liangdeng
# Email:       87961327@QQ.COM
# Description: 
# Notes: 
# -------------------------------------------------------------------------------
cd `dirname $0`
BIN_DIR=`pwd`
cd ..
DEPLOY_DIR=`pwd`

#PIDS=`jps | grep 服务名 | awk '{print $1}'`
PIDS=`ps -ef | grep java | grep "$DEPLOY_DIR" |awk '{print $2}'`
if [ -z "$PIDS" ]; then
    echo "ERROR: The Service does not started!"
    exit 1
fi
echo -e "Stopping the Service ...\c"
for PID in $PIDS ; do
    kill -9 $PID > /dev/null 2>&1
done

COUNT=0
while [ $COUNT -lt 1 ]; do    
    echo -e ".\c"
    sleep 1
    COUNT=1
    for PID in $PIDS ; do
        PID_EXIST=`ps -f -p $PID | grep java`
        if [ -n "$PID_EXIST" ]; then
            COUNT=0
            break
        fi
    done
done

echo "Service stop OK!"
echo > $BIN_DIR/pid
echo "PID: $PIDS"
