package com.california.pay.common.cache;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.util.Assert;

import java.util.Arrays;


@Slf4j
public abstract class AbstractCache<T> implements Cache {
    /**
     * 根据关键字（可多个，拼接方式成一个），获取配置
     * 子类应该基于此方法实现一个更友好的获取方法
     *
     * @param keys
     * @return
     */
    @Override
    public T get(String... keys) {
        if (keys == null || keys.length == 0) {
            throw new IllegalArgumentException("require at least one key.");
        }

        for (String key : keys) {
            Assert.notNull(key, "keys can't be null");
        }

        T value = null;
        //缓存失败不影响程序运行
        try {
            value = getValue(getKey(keys));
        } catch (Throwable t) {
            log.warn("can't get value from cache, keys=" + Arrays.toString(keys), t);
        }

        if (value == null) {
            value = getFromRepository(keys);
            if (value != null) {
                //缓存失败不影响程序运行
                try {
                    setValue(getKey(keys), value);
                } catch (Throwable t) {
                    log.warn("can't put value to cache, value=" + value, t);
                }
            }
        }
        return value;
    }

    /**
     * 清空缓存
     */
    @Override
    public abstract void clearCache();

    /**
     * 获取关键字的分隔符
     */
    protected String getKeySplitter() {
        return ":";
    }

    /**
     * 从持久设备上获取数据，由子类实现
     *
     * @param keys
     * @return
     */
    protected abstract T getFromRepository(String... keys);

    /**
     * 获取缓存的名字，每个缓存名称必须唯一， 默认为实现类的类名
     *
     * @return
     */
    protected String getCacheName() {
        return "cache:" + this.getClass().getName();
    }

    /**
     * 获取关键字
     *
     * @param keys
     * @return
     */
    protected String getKey(String... keys) {
        StringBuffer sbKey = new StringBuffer();
        for (String key : keys) {
            sbKey.append(key).append(getKeySplitter());
        }
        sbKey.setLength(sbKey.length() - 1);

        return sbKey.toString();
    }

    /**
     * 根据组合的关键字获取缓存值
     *
     * @param keys
     * @return
     */
    protected abstract T getValue(String keys);

    /**
     * 将值设到组合关键字的缓存中
     *
     * @param keys
     * @param value
     */
    protected abstract void setValue(String keys, T value);

    /**
     * 获取Redisson缓存代理
     *
     * @return
     */
    protected abstract RedissonClient getRedissonClient();
}
