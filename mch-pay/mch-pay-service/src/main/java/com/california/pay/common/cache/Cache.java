package com.california.pay.common.cache;

/**
 * 缓存
 */
public interface Cache<T> {
    /**
     * 根据关键字（可多个，拼接方式成一个），获取配置
     * 子类应该基于此方法实现一个更友好的获取方法
     * <p>
     * 注意，该方法不能被事务拦截，否则每次调用此方法，都会连接/释放数据库链接，造成取缓存耗时加长
     *
     * @param keys
     * @return
     */
    T get(String... keys);

    /**
     * 清空缓存
     */
    void clearCache();
}
