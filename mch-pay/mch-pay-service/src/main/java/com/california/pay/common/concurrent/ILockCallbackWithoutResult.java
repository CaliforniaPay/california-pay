package com.california.pay.common.concurrent;

/**
 * 无返回结果的锁回调操作.
 */
@FunctionalInterface
public interface ILockCallbackWithoutResult {
    /**
     * 执行业务逻辑
     *
     * @param locked 是否加锁成功
     * @return
     */
    void doWithLock(boolean locked);
}
