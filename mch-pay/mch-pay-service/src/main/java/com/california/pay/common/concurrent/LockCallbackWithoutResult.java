package com.california.pay.common.concurrent;

/**
 * 锁回调不带返回值
 */
public abstract class LockCallbackWithoutResult implements LockCallback<Object> {
    /**
     * @param locked 是否加锁成功
     * @return
     */
    @Override
    public Object doWithLock(boolean locked) {
        doWithLockWithoutResult(locked);
        return null;
    }

    /**
     * 在加锁下操作，不返回结果
     *
     * @param locked
     */
    protected abstract void doWithLockWithoutResult(boolean locked);
}
