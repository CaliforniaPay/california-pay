package com.california.pay.common.dubbo.filter;

import com.alibaba.dubbo.rpc.*;
import com.california.pay.common.trace.TraceThreadLocal;

/**
 * 设置全局ID为线程号，跟踪调用过程
 */
public class TraceFilter implements Filter {
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String traceId = TraceThreadLocal.getTraceId();
        //是否起始跟踪位置
        boolean isBeginning = false;
        // 说明方法准备往外调用，即当前为consumer
        if (traceId != null) {
            //将traceId传到服务端
            invocation.getAttachments().put(TraceThreadLocal.TRACE_ID, traceId);
        } else {
            traceId = invocation.getAttachment(TraceThreadLocal.TRACE_ID);

            // 如果非本地调用，则说明方法是从外面调进来的，即当前为provider
            if (traceId != null) {
                TraceThreadLocal.setTraceId(traceId);
                //那么此时此处是起始跟踪位置
                isBeginning = true;
            }
        }

        try {
            return invoker.invoke(invocation);
        } finally {
            //如果跟踪是从此处开始的，那么最后跟踪结束时，需要移除局部变量
            if (isBeginning) {
                TraceThreadLocal.clear();
            }
        }
    }
}
