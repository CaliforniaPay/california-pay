package com.california.pay.common.id;

/**
 * ID生成器
 */
public interface IdWorker {
    /**
     * 按名字获取下一个id值
     *
     * @param name 流水名称
     * @return
     */
    long nextId(String name);

    /**
     * 计算id中生成器的workId字段值.
     * @author zhouwenqing
     * @date 2018/11/29
     * @param id
     * @return int
     */
    int getWorkId(String id);

    /**
     * 用全局统一的名字获取id值
     *
     * @return
     */
    long nextId();

    /**
     * 返回string格式的id
     *
     * @param name 流水名称
     * @return
     */
    default String nextIdString(String name) {
        return nextId(name) + "";
    }

    /**
     * 返回string格式的id
     *
     * @return
     */
    default String nextIdString() {
        return nextId() + "";
    }
}
