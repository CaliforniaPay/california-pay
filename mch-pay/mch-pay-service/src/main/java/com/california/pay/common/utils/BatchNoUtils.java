package com.california.pay.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 生成批次号
 */
public class BatchNoUtils {

    /**
     * 生成批次号
     */
    public static long genBatchNoBy5Min(Date time) {
        String date = DateUtils.yyyyMMdd(time);
        String hh = DateUtils.HH(time);
        String mm = DateUtils.mm(time);

        // 批次号从1开始
        int batch = (Integer.parseInt(hh) * 60 + Integer.parseInt(mm)) / 5 + 1;
        String batchStr = StringUtils.leftPad(String.valueOf(batch), 3, '0');

        return Long.parseLong(date + batchStr);
    }

    /**
     * 每分钟生成批次好
     *
     * @param time
     * @return
     */
    public static long genBatchNoBy1Min(Date time) {
//        String date = DateUtils.yyyyMMdd(time);
//        String hh = DateUtils.HH(time);
//        String mm = DateUtils.mm(time);
//
//        // 批次号从1开始
//        int batch = (Integer.parseInt(hh) * 60 + Integer.parseInt(mm)) + 1;
//        String batchStr = StringUtils.leftPad(String.valueOf(batch), 3, '0');
        String timeStr = DateUtils.yyyyMMddHHmm(time);

        return Long.parseLong(timeStr);
    }

    /**
     * 下次执行时间
     */
    public static Date getNextTime(Date now, int times) {
        //double minutes = Math.pow(2.0, (double) (times - 1)) * 5;
        int minutes = (2 * times - 1) * 5;
        return DateUtils.addMinute(now, minutes);
    }

    /**
     * 获取下一个5分钟
     */
    public static Date getNext5MinTime(Date now) {
        return DateUtils.addMinute(now, 5);
    }

    public static Date getNext10MinTime(Date now) {
        return DateUtils.addMinute(now, 10);
    }

    public static void main(String[] args) {
        System.out.println(genBatchNoBy5Min(new Date()));

        Date now = DateUtils.stringToDate("20180718230059", "yyyyMMddHHmmss");
        System.out.println(genBatchNoBy5Min(now));

        System.out.println(getNextTime(now, 1));
    }
}
