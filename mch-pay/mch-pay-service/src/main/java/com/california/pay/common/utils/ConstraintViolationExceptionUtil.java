package com.california.pay.common.utils;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Iterator;
import java.util.Set;

public class ConstraintViolationExceptionUtil {


    public static String getConstraintViolationExceptionErrMsg(ConstraintViolationException e) {
        StringBuffer sb = new StringBuffer();
        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
        Iterator i$ = constraintViolations.iterator();

        while (i$.hasNext()) {
            ConstraintViolation constraintViolation = (ConstraintViolation) i$.next();
            sb.append(StringUtils.isEmpty(constraintViolation.getMessage()) ? constraintViolation.toString() : constraintViolation.getMessage()).append("\r\n");
        }
        return sb.toString();

    }
}
