package com.california.pay.config;

/**
 * 定义rediskey
 */
public class LockConsts {
    /**
     * 锁等待时间
     */
    public final static int LOCK_WAIT_TIME = 0;
    /**
     * 锁释放时间
     */
    public final static int LOCK_LEASE_TIME = 60 * 1000;

    public final static String TRADE_AUTH_KEY = "trade_auth_key:";

    public final static String TRADE_FINISH_KEY = "trade_finish_key:";

    public final static String TRADE_CANCEL_KEY = "trade_cancel_key:";

    public final static String TRADE_REFUND_KEY = "trade_refund_key:";
}
