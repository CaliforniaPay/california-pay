package com.california.pay.config;

/**
 * 内部MQ相关配置
 */
public class MQInnerConfig {


    //----------------- tag定义 -----------------

    /**
     * 交易授权
     */
    public static final String TAG_CHANNEL_TRADE_AUTHED = "TAG_CHANNEL_TRADE_AUTHED";


    //----------------- topic定义 -----------------

    /**
     * topic 共用
     */
    public static final String TOPIC_CHANNEL_PAY = "TOPIC_CHANNEL_PAY";
}
