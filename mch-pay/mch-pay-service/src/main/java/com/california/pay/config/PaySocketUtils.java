package com.california.pay.config;

import com.bwton.socket.transport.call.Response;
import com.bwton.socket.transport.call.SocketMessage;
import com.bwton.socket.util.EncodeUtil;
import com.california.pay.common.utils.DateUtils;
import com.california.pay.common.trace.TraceThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;


/**
 * socket 工具类
 */
@Slf4j
public class PaySocketUtils {
    private static final Logger SOCKET_CLIENT_LOG = LoggerFactory.getLogger("SOCKET_CLIENT_LOG");

    /**
     * 设置跟踪流水号
     */
    public static void initTraceId(String requestId) {
        String traceId = TraceThreadLocal.getTraceId();
        if (traceId == null) {
            traceId = requestId + "#" + DateUtils.yyyyMMddHHmmss(new Date());
        }

        TraceThreadLocal.setTraceId(traceId);
    }

    /**
     * 设置跟踪流水号
     */
    public static void initTraceId(String requestId, boolean isNewTraceId) {
        String traceId = TraceThreadLocal.getTraceId();
        if (traceId == null || isNewTraceId) {
            traceId = requestId + "#" + DateUtils.yyyyMMddHHmmss(new Date());
        }

        TraceThreadLocal.setTraceId(traceId);
    }

    /**
     * 清除跟踪流水号
     */
    public static void clearTraceId() {
        TraceThreadLocal.clear();
    }

    /**
     * 打印请求日志
     */
    public static void logRequestMsg(Map<String, String> dataMap, byte[] tpduBts, byte[] headerBts, String mti, String logTip) {
        if (SOCKET_CLIENT_LOG.isDebugEnabled()) {

        }
    }

    /**
     * 打印响应日志
     */
    public static void logResponse(Response response, String requestId, String logTip) {
        if (SOCKET_CLIENT_LOG.isDebugEnabled()) {

        }
    }

    /**
     * 打印收到报文
     *
     * @param remoteAddress
     * @param socketMessage
     */
    public static void printRecvSocketMessage(String remoteAddress, SocketMessage socketMessage) {
        if (SOCKET_CLIENT_LOG.isDebugEnabled()) {
            String line = EncodeUtil.getOsSeparator();
            byte[] msg = socketMessage.getData();
            StringBuilder tmp = new StringBuilder();
            tmp.append(line).append("#####收到报文(十六进制), url=" + remoteAddress + "#####").append(line);
            tmp.append(Hex.encodeHexString(msg)).append(line);
            SOCKET_CLIENT_LOG.debug(tmp.toString());
        }
    }

    /**
     * 打印发送报文
     *
     * @param remoteAddress
     * @param socketMessage
     */
    public static void printSendSocketMessage(String remoteAddress, SocketMessage socketMessage) {
        if (SOCKET_CLIENT_LOG.isDebugEnabled()) {
            String line = EncodeUtil.getOsSeparator();
            StringBuilder tmp = new StringBuilder();
            tmp.append(line).append("#####发送报文(十六进制), uri=" + remoteAddress + "#####").append(line);
            tmp.append(Hex.encodeHexString(socketMessage.getData())).append(line);
            SOCKET_CLIENT_LOG.debug(tmp.toString());
        }
    }

}
