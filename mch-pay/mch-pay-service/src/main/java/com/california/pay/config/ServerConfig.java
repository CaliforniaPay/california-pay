package com.california.pay.config;


import com.california.pay.common.properties.PropertiesLoader;

/**
 */
public class ServerConfig {
    public static final String DEFAULT_CHARSET = "utf-8";
    /**
     * restTemplate连接池参数
     */
    public static final int HTTP_MAX_TOTAL;
    public static final int HTTP_MAX_PERROUTE;
    public static final long HTTP_CONNE_TIME_TO_LIVE;
    //public static final Long PERSON_MCH_MAX_QUOTA;
    public static PropertiesLoader propertiesLoader;

    static {

        propertiesLoader = new PropertiesLoader("classpath*:app.properties");

        HTTP_MAX_TOTAL = propertiesLoader.getInteger("http.maxTotal", 400);
        HTTP_MAX_PERROUTE = propertiesLoader.getInteger("http.defaultMaxPerRoute", 400);
        HTTP_CONNE_TIME_TO_LIVE = Long.parseLong(propertiesLoader.getProperty("http.timeToLive", "30000"));

        //PERSON_MCH_MAX_QUOTA = Long.parseLong(propertiesLoader.getProperty("person.mch.max.quota", "20000000"));
    }

}
