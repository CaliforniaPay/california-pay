package com.california.pay.config;

/**
 */
public enum UnionParamConfig {
    /**
     * 是否采用模拟调用银联闪付
     */
    unionQpMockInvoke("union.qp.mock.invoke", false),;

    private String name;
    private String value;
    private long longValue;
    private int intValue;
    private boolean boolValue;

    private UnionParamConfig(String name, String value) {
        this.name = name;
        this.value = value;
    }

    private UnionParamConfig(String name, long longValue) {
        this.name = name;
        this.value = String.valueOf(longValue);
        this.longValue = longValue;
    }

    private UnionParamConfig(String name, int intValue) {
        this.name = name;
        this.value = String.valueOf(intValue);
        this.intValue = intValue;
    }

    private UnionParamConfig(String name, boolean boolValue) {
        this.name = name;
        this.value = String.valueOf(boolValue);
        this.boolValue = boolValue;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public int getIntValue() {
        return this.intValue;
    }

    public long getLongValue() {
        return this.longValue;
    }

    public boolean getBooleanValue() {
        return this.boolValue;
    }


}
