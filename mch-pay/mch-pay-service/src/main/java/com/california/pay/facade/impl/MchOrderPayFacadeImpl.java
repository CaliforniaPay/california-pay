package com.california.pay.facade.impl;

import com.alibaba.fastjson.JSON;
import com.california.pay.config.FacadeServiceTemplate;
import com.california.pay.facade.MchOrderPayFacade;
import com.california.pay.model.OrderPayNotifyResultReq;
import com.california.pay.model.OrderPayNotifyResultRsp;
import com.california.pay.model.PersonOrderPayReq;
import com.california.pay.model.PersonOrderPayRsp;
import com.california.pay.result.CommonResult;
import com.california.pay.service.BaseService;
import com.california.pay.service.OrderPayService;
import com.california.pay.service.OrderSupplyService;
import com.california.pay.service.PayNotifyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;


@Slf4j
public class MchOrderPayFacadeImpl implements MchOrderPayFacade {
    @Autowired
    private FacadeServiceTemplate serviceTemplate;
    @Autowired
    private OrderPayService payService;
    @Autowired
    private PayNotifyService payNotifyService;
    @Autowired
    private OrderSupplyService orderSupplyService;
    private String simpleClassName = this.getClass().getSimpleName();

    @Override
    public CommonResult<PersonOrderPayRsp> personMchOrderPay(PersonOrderPayReq orderPayReq) {
        BaseService.initTraceId(simpleClassName + "#personMchOrderPay");
        String facadeName = "[生成支付订单#MchOrderPayFacade#personMchOrderPay]接口";
        log.info("请求{}, 请求报文={}", facadeName, JSON.toJSONString(orderPayReq));
        CommonResult<PersonOrderPayRsp> result = serviceTemplate.execute(facadeName, () -> payService.personMchOrderPay(orderPayReq));
        log.info("请求{}, 应答报文={}", facadeName, JSON.toJSONString(result));
        return result;
    }

    @Override
    public CommonResult<OrderPayNotifyResultRsp> orderpayNotifyAck(OrderPayNotifyResultReq notifyResultReq) {
        BaseService.initTraceId(simpleClassName + "#orderpayNotifyAck");
        String facadeName = "[商户通知应答#MchOrderPayFacade#orderpayNotifyAck]接口";
        log.info("请求{}, 请求报文={}", facadeName, JSON.toJSONString(notifyResultReq));
        CommonResult<OrderPayNotifyResultRsp> result = serviceTemplate.execute(facadeName, () -> payNotifyService.mchNotifyAck(notifyResultReq));
        log.info("请求{}, 应答报文={}", facadeName, JSON.toJSONString(result));
        return null;
    }

    @Override
    public CommonResult<?> supplyPayOrder(String payOrderId) {
        BaseService.initTraceId(simpleClassName + "#supplyPayOrder");
        String facadeName = "[补单#MchOrderPayFacade#supplyPayOrder]接口";
        log.info("请求{}, 请求报文={}", facadeName, JSON.toJSONString(payOrderId));
        CommonResult<String> result = serviceTemplate.execute(facadeName, () -> orderSupplyService.supplyPayOrder(payOrderId));
        log.info("请求{}, 应答报文={}", facadeName, JSON.toJSONString(result));
        return result;
    }




}
