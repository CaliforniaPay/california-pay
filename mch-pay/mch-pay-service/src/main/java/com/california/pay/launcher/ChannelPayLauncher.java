package com.california.pay.launcher;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 启动类
 */
@Service
public class ChannelPayLauncher implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger logger = LoggerFactory.getLogger("");

    public static void main(String[] args) throws IOException {
        com.alibaba.dubbo.container.Main.main(args);
    }

    private static void logBanner() throws IOException {
        ClassPathResource cpr = new ClassPathResource("banner.txt");
        String banner = IOUtils.toString(cpr.getURI(), "utf-8");
        banner += getLocalip();
        logger.info(banner);
        getLocalip();
    }

    private static String getLocalip() {
        try {
            return ":: [Dubbo]服务暴露的地址: "
                    + java.net.InetAddress.getLocalHost().getHostAddress() ;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return "";
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getApplicationContext().getParent() == null) {
            try {
                logBanner();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

