package com.california.pay.persist.domain;

import com.california.pay.consts.Status;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "p_mch_info")
public class MchInfo implements Serializable {
    /**
     * 商户ID
     */
    @Id
    @Column(name = "mch_id")
    private String mchId;

    /**
     * 商户编码
     */
    @Column(name = "mch_no")
    private String mchNo;

    /**
     * 名称
     */
    @Column(name = "mch_name")
    private String mchName;

    /**
     * 类型
     */
    @Column(name = "type")
    private String type;

    /**
     * 请求私钥
     */
    @Column(name = "req_key")
    private String reqKey;

    /**
     * 响应私钥
     */
    @Column(name = "res_key")
    private String resKey;

    /**
     * 商户状态,0-停止使用,1-使用中
     */
    @Column(name = "state")
    private Status state;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

}