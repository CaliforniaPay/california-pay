package com.california.pay.persist.domain;

import com.california.pay.consts.Status;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
@Data
@Table(name = "p_mch_pay_quota")
public class MchPayQuota implements Serializable {
    /**
     * 渠道主键ID
     */
    @Id
    @Column(name = "quota_id")
    private String quotaId;

    /**
     * 商户或个人渠道ID
     */
    @Column(name = "channel_mch_id")
    private String channelMchId;

    /**
     * 已支付总金额
     */
    @Column(name = "pay_total_amount")
    private Long payTotalAmount;

    /**
     * 支付时间，格式：yyyyMMdd
     */
    @Column(name = "pay_date")
    private Long payDate;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "version")
    private Integer version;

    @Column(name = "status")
    private Status status;


    private static final long serialVersionUID = 1L;

}