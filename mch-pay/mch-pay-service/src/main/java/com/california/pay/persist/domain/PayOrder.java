package com.california.pay.persist.domain;

import com.california.pay.consts.ChannelMchType;
import com.california.pay.consts.NotifyStatus;
import com.california.pay.consts.OrderStatus;
import com.california.pay.consts.PayResultTrigger;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "p_pay_order")
public class PayOrder implements Serializable {
    /**
     * 支付订单ID
     */
    @Id
    @Column(name = "order_id")
    private String orderId;

    /**
     * 支付订单号
     */
    @Column(name = "inner_order_no")
    private String innerOrderNo;

    /**
     * 商户ID
     */
    @Column(name = "inner_mch_no")
    private String innerMchNo;

    /**
     * 商户订单号
     */
    @Column(name = "mch_order_no")
    private String mchOrderNo;

    /**
     * 请求总支付金额,单位分
     */
    @Column(name = "total_amount")
    private Long totalAmount;

    @Column(name = "txn_amount")
    private Long txnAmount;

    /**
     * 三位货币代码,人民币:cny
     */
    @Column(name = "currency")
    private String currency;


    /**
     * 支付状态,0-订单生成,1-支付中(目前未使用),2-支付成功,3-订单完成(不可退款), 4-订单失败关闭
     */
    @Column(name = "order_status")
    private OrderStatus orderStatus;

    /**
     * 订单失效时间
     */
    @Column(name = "order_expire_time")
    private Long orderExpireTime;

    /**
     * 订单生成时间
     */
    @Column(name = "order_gen_time")
    private Long orderGenTime;

    /**
     * 订单支付成功时间,格式yyyyMMddhhmmss
     */
    @Column(name = "pay_succ_time")
    private Long paySuccTime;

    /**
     * 客户端IP
     */
    @Column(name = "client_ip")
    private String clientIp;

    /**
     * 设备
     */
    @Column(name = "device")
    private String device;

    /**
     * 终端设备
     */
    @Column(name = "terminal_id")
    private String terminalId;

    /**
     * 商品标题
     */
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 商品描述信息
     */
    @Column(name = "goods_desc")
    private String goodsDesc;

    /**
     * 特定渠道发起时额外参数
     */
    @Column(name = "extra")
    private String extra;

    /**
     * 渠道商户ID
     */
    @Column(name = "channel_mch_id")
    private String channelMchId;

    /**
     * 渠道商户类型
     */
    @Column(name = "channel_mch_type")
    private ChannelMchType channelMchType;

    /**
     * 渠道appID
     */
    @Column(name = "channel_app_id")
    private String channelAppId;

    /**
     * 渠道appID
     */
    @Column(name = "channel_user_id")
    private String channelUserId;

    /**
     * 渠道订单号
     */
    @Column(name = "channel_order_no")
    private String channelOrderNo;

    /**
     * 渠道支付错误码
     */
    @Column(name = "channel_err_code")
    private String channelErrCode;

    /**
     * 渠道支付错误描述
     */
    @Column(name = "channel_err_msg")
    private String channelErrMsg;

    /**
     * 渠道最后通知时间, 格式yyyyMMddhhmmss
     */
    @Column(name = "channel_last_notify_time")
    private Long channelLastNotifyTime;

    /**
     * 扩展参数1
     */
    @Column(name = "param1")
    private String param1;

    /**
     * 扩展参数2
     */
    @Column(name = "param2")
    private String param2;

    /**
     * 通知地址
     */
    @Column(name = "mch_notify_url")
    private String mchNotifyUrl;

    /**
     * 同步URL
     */
    @Column(name = "mch_return_url")
    private String mchReturnUrl;

    /**
     * 通知次数
     */
    @Column(name = "mch_notify_count")
    private Integer mchNotifyCount;

    /**
     * SUCC, TIMEOUT, FAIL
     */
    @Column(name = "mch_notify_status")
    private NotifyStatus mchNotifyStatus;

    @Column(name = "mch_return_status")
    private NotifyStatus mchReturnStatus;

    /**
     * 最后一次通知时间, 格式yyyyMMddhhmmss
     */
    @Column(name = "last_mch_notify_time")
    private Long lastMchNotifyTime;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;



    @Column(name = "pay_date")
    private Long payDate;



    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "pay_result_trigger")
    private PayResultTrigger payResultTrigger;

    private static final long serialVersionUID = 1L;
}