package com.california.pay.persist.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "p_pay_org_mch")
public class PayOrgMch implements Serializable {
    /**
     * 渠道主键ID
     */
    @Id
    @Column(name = "org_mch_id")
    private String orgMchId;

    /**
     * alipay_qr
     */
    @Column(name = "channel_no")
    private String channelNo;

    /**
     * 渠道名称,如:alipay,wechat
     */
    @Column(name = "channel_name")
    private String channelName;

    /**
     * 渠道商户ID, 或者个人支付宝账号
     */
    @Column(name = "channel_mch_id")
    private String channelMchId;

    /**
     * 渠道商户ID
     */
    @Column(name = "channel_app_id")
    private String channelAppId;

    /**
     * 渠道状态,0-停止使用,1-使用中
     */
    private Byte state;

    /**
     * ali公钥
     */
    @Column(name = "channel_pub_key")
    private String channelPubKey;

    /**
     * 自己公钥
     */
    @Column(name = "self_pub_key")
    private String selfPubKey;

    /**
     * 自己私钥
     */
    @Column(name = "self_prv_key")
    private String selfPrvKey;

    /**
     * 0-正式环境，1-沙箱
     */
    @Column(name = "sand_box")
    private Byte sandBox;

    /**
     * 单日最大收款金额
     */
    @Column(name = "max_amount")
    private Long maxAmount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}