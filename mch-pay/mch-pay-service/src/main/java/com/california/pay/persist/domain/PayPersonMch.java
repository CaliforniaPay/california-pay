package com.california.pay.persist.domain;

import com.california.pay.consts.ChannelType;
import com.california.pay.consts.Status;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "p_pay_person_mch")
public class PayPersonMch implements Serializable {
    /**
     * 个人渠道主键ID
     */
    @Id
    @Column(name = "person_mch_id")
    private String personMchId;

    /**
     * 渠道编码，alipay_person
     */
    @Column(name = "channel_no")
    private ChannelType channelNo;

    /**
     * 渠道名称,如:alipay,wechat
     */
    @Column(name = "channel_name")
    private String channelName;

    /**
     * 渠道个人账号, 或者个人支付宝账号
     */
    @Column(name = "channel_mch_account")
    private String channelMchAccount;

    /**
     * 渠道用户ID
     */
    @Column(name = "channel_user_id")
    private String channelUserId;

    /**
     * 渠道状态,0-停止使用,1-使用中
     */
    @Column(name = "state")
    private Status state;

    /**
     * 单日最大收款金额
     */
    @Column(name = "max_amount")
    private Long maxAmount;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private static final long serialVersionUID = 1L;

}