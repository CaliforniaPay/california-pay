package com.california.pay.persist.domain;

import com.california.pay.consts.Status;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "p_terminal")
public class Terminal implements Serializable {
    /**
     * 商户ID
     */
    @Id
    @Column(name = "terminal_id")
    private String terminalId;

    /**
     * 终端标识
     */
    @Column(name = "client_id")
    private String clientId;

    /**
     * 签名私钥
     */
    @Column(name = "app_key")
    private String appKey;

    /**
     * 状态,0-可用，1：不可用
     */
    @Column(name = "state")
    private Status state;

    /**
     * 状态,0-苹果,1-安卓
     */
    @Column(name = "terminal_type")
    private String terminalType;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}