package com.california.pay.persist.domain;

import com.california.pay.consts.LoginStatus;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "p_terminal_login")
public class TerminalLogin implements Serializable {
    /**
     * 渠道主键ID
     */
    @Id
    @Column(name = "login_id")
    private String loginId;

    /**
     * alipay_qr
     */
    @Column(name = "channel_mch_id")
    private String channelMchId;

    /**
     * 渠道名称,如:alipay,wechat
     */
    @Column(name = "channel_user_id")
    private String channelUserId;

    @Column(name = "terminal_id")
    private String terminalId;

    /**
     * 渠道商户ID, 或者个人支付宝账号
     */
    @Column(name = "client_id")
    private String clientId;

    /**
     * 登录会话凭证
     */
    @Column(name = "session_id")
    private String sessionId;

    /**
     * 登录会话凭证
     */
    @Column(name = "app_key")
    private String appKey;

    /**
     * 渠道商户ID
     */
    @Column(name = "login_time")
    private Date loginTime;

    /**
     * 渠道商户ID
     */
    @Column(name = "logout_time")
    private Date logoutTime;

    /**
     * 渠道状态,0-登录中,1-登出
     */
    @Column(name = "login_status")
    private LoginStatus loginStatus;

    /**
     * 备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "netty_channel_id")
    private String nettyChannelId;

    @Column(name = "client_heartbeat_time")
    private Date clientHeartbeatTime;

    @Column(name = "recv_heartbeat_time")
    private Date recvHeartbeatTime;

    @Column(name = "login_out_time")
    private Date loginOutTime;


    private static final long serialVersionUID = 1L;

}