package com.california.pay.persist.mapper;

import com.california.pay.persist.domain.MchInfo;
import tk.mybatis.mapper.common.Mapper;

public interface MchInfoMapper extends Mapper<MchInfo> {
}