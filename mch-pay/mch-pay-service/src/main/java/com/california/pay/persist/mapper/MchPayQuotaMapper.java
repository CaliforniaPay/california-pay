package com.california.pay.persist.mapper;

import com.california.pay.consts.Status;
import com.california.pay.persist.domain.MchPayQuota;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface MchPayQuotaMapper extends Mapper<MchPayQuota> {

    public Long getMinPayAmount(@Param("payDate") Long payDate, @Param("payAmount") Long payAmount);

    public MchPayQuota getMinPayAmountRecord(@Param("payDate") Long payDate, @Param("minPayAmount") Long minPayAmount);

    public int addPayAmountByPrimaryKey(@Param("txnAmount")Long txnAmount, @Param("quotaId")String quotaId);

    public MchPayQuota selectByPrimaryKeyForUpdate(@Param("quotaId") String quotaId);

    public int updateStatusByChannelMchId(@Param("status") Status status, @Param("channelMchId")String channelMchId, @Param("payDate") Long payDate);

    public MchPayQuota selectOneByMchIdAndPayDate(@Param("payDate") Long payDate, @Param("channelMchId")String channelMchId);
}