package com.california.pay.persist.mapper;

import com.california.pay.persist.domain.PayOrder;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface PayOrderMapper extends Mapper<PayOrder> {

    /**
     * 获取通知商户失败的订单
     * @param minNotifyDate
     * @param totalNotifyCount
     * @return
     */
    public List<PayOrder> getNotifyMchFailList(@Param("minNotifyDate") Long minNotifyDate, @Param("totalNotifyCount") Integer totalNotifyCount);

    public int updateOrderStatusToFail(@Param("nowTime") Long nowTime);
}