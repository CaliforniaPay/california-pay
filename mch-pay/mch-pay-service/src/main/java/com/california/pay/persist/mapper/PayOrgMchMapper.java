package com.california.pay.persist.mapper;

import com.california.pay.persist.domain.PayOrgMch;
import tk.mybatis.mapper.common.Mapper;

public interface PayOrgMchMapper extends Mapper<PayOrgMch> {
}