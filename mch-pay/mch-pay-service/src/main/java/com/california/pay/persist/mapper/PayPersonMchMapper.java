package com.california.pay.persist.mapper;

import com.california.pay.persist.domain.PayPersonMch;
import tk.mybatis.mapper.common.Mapper;

public interface PayPersonMchMapper extends Mapper<PayPersonMch> {
}