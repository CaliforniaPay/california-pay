package com.california.pay.persist.mapper;

import com.california.pay.persist.domain.TerminalLogin;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface TerminalLoginMapper extends Mapper<TerminalLogin> {

    public int deleteByChannelMchId(@Param("channelMchId") String channelMchId);

    public TerminalLogin selectOneByChannelId(String nettyChannelId);

    public TerminalLogin selectOneByChannelUserId(String channelUserId);
}