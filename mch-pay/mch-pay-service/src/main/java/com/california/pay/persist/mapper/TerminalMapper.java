package com.california.pay.persist.mapper;

import com.california.pay.persist.domain.Terminal;
import tk.mybatis.mapper.common.Mapper;

public interface TerminalMapper extends Mapper<Terminal> {
}