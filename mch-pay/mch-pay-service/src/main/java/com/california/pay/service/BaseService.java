package com.california.pay.service;


import com.california.pay.common.trace.TraceThreadLocal;
import com.california.pay.common.validator.BeanValidators;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.Validator;

public abstract class BaseService {
    @Autowired
    private Validator validator;

    public BaseService() {
    }

    public static void initTraceId(String tip) {
        //String traceId = TraceThreadLocal.getTraceId();

        //if (StringUtils.isBlank(traceId)) {
        String traceId = tip + "#" + RandomUtils.nextInt();
        TraceThreadLocal.setTraceId(traceId);
        //}

    }

    protected void validate(Object object, Class... groups) {
        BeanValidators.validateWithException(this.validator, object, groups);
    }
}
