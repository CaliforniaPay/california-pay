package com.california.pay.service;

import com.california.pay.config.CommonErrors;
import com.california.pay.exception.BusinessException;
import com.california.pay.persist.domain.TerminalLogin;
import com.california.pay.persist.mapper.TerminalLoginMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author XXXXX
 * @date 2018/12/15
 */
@Slf4j
@Service
public class SessionService {

    @Autowired
    private TerminalLoginMapper loginMapper;
    @Autowired
    private TerminalLoginService  loginService;

    public void validateSession(String clientId, String sessionId) {
        TerminalLogin login = loginService.getTerminalLogin(clientId);
        if (!StringUtils.equals(sessionId, login.getSessionId())){
            throw new BusinessException(CommonErrors.SESSION_ERROR, "客户端sessionId错误，req_sessionId=" + sessionId + ", clientId=" + clientId);
        }
    }

}
