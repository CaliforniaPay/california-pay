package com.california.pay.socket;

import com.bwton.socket.transport.call.SocketMessage;
import lombok.Data;

import java.nio.charset.Charset;

/**
 * 报文消息对象, command_data, command_data_params 二者选其一，command_data优化判断
 *
 * @author liangd.chen
 * @date 2018/12/6
 */
@Data
public class TransSocketMessage extends SocketMessage {
    public static final String APP_ID = "appId";
    public static final String VERSION = "version";
    public static final String SESSION_ID = "sessionId";
    public static final String SEQ = "seq";
    public static final String COMMAND_CODE = "commandCode";
    public static final String TIMESTAMP = "timestamp";
    public static final String RETURN_CODE = "returnCode";
    public static final String MESSAGE = "message";
    public static final String SIGN_METHOD = "signMethod";
    public static final String SIGNATURE = "signature";
    private static final long serialVersionUID = -2582929655311804192L;
    private String appId;
    private String version;
    private String sessionId;
    private String seq;
    private String commandCode;
    private String timestamp;
    private String returnCode;
    private String message;
    private String signMethod;
    private String signature;


    /**
     * 总消息报文
     */
    private String socketBody;

    public TransSocketMessage() {
        super();
    }

    public TransSocketMessage(boolean isRequest, byte[] data) {
        super(isRequest, data);
        this.socketBody = new String(data, Charset.forName("utf-8"));
        parse(this.socketBody);
    }

    @Override
    public String getRequestId() {
        return this.sessionId + "^" + this.seq;
    }

    @Override
    public String getServiceKey() {
        return this.commandCode;
    }

    public void parse(String socketBody) {
        String[] subs = socketBody.split("&");
        for (String sub : subs) {
            String[] subSubs = sub.split("=");
            if (subSubs.length < 2) {
                continue;
            }
            String name = subSubs[0];
            String value = subSubs[1];


            if (APP_ID.equals(name)) {
                this.appId = value;
            } else if (VERSION.equals(name)) {
                this.version = value;
            } else if (SESSION_ID.equals(name)) {
                this.sessionId = value;
            } else if (SEQ.equals(name)) {
                this.seq = value;
            } else if (COMMAND_CODE.equals(name)) {
                this.commandCode = value;
            } else if (TIMESTAMP.equals(name)) {
                this.timestamp = value;
            } else if (RETURN_CODE.equals(name)) {
                this.returnCode = value;
            } else if (MESSAGE.equals(name)) {
                this.message = value;
            } else if (SIGN_METHOD.equals(name)) {
                this.signMethod = value;
            } else if (SIGNATURE.equals(name)) {
                this.signature = value;
            }


        }
    }

    public String getOriginBody() {
        StringBuilder tmp = new StringBuilder();
        tmp.append("appId=").append(this.appId).append("&")
                .append("version=").append(this.version).append("&")
                .append("sessionId=").append(this.sessionId).append("&")
                .append("seq=").append(this.seq).append("&")
                .append("commandCode=").append(this.commandCode).append("&")
                .append("timestamp=").append(this.timestamp).append("&")
                .append("returnCode=").append(this.returnCode).append("&")
                .append("message=").append(this.message).append("&")
                .append("signMethod=").append(this.signMethod);
        return tmp.toString();
    }
}
