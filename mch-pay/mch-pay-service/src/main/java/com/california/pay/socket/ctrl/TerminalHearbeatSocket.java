package com.california.pay.socket.ctrl;

import com.bwton.socket.transport.SocketController;
import com.bwton.socket.transport.call.Request;
import com.bwton.socket.transport.call.Response;
import com.california.pay.config.CommonErrors;
import com.california.pay.exception.BusinessException;
import com.california.pay.service.TerminalHeartbeatService;
import com.california.pay.socket.SocketServerProcessorAdapter;
import com.california.pay.socket.TransSocketMessage;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Slf4j
@SocketController(servicekey = "B1", version = "1.0")
public class TerminalHearbeatSocket extends SocketServerProcessorAdapter {
    @Autowired
    private TerminalHeartbeatService heartbeatService;

    @Override
    public Response doHandle(Request request, Response response) {
        super.doHandle(request, response);
        TransSocketMessage reqMessage = (TransSocketMessage) request.getTms();
        log.info("终端心跳报文:{}", reqMessage.getSocketBody());
        String returnCode = CommonErrors.SUCCESS.getCode();
        Map<String, String> rspMap = Maps.newHashMap();

        try {
            rspMap = heartbeatService.clientHeaxrtbeat(reqMessage, request.getNettyChannel());
        } catch (Exception e) {
            log.info("终端心跳处理异常, message=" + e.getMessage(), e);
            if (e instanceof BusinessException){
                returnCode = ((BusinessException)e).getCode();
            }else{
                returnCode = CommonErrors.UNDEFINED.getCode();
            }
        }

        buildBizResponse(returnCode, rspMap, reqMessage, response);
        log.info(">>>>>>>终端心跳处理成功【{}】", returnCode);
        return response;
    }



}
