package com.california.pay.socket.ctrl;

import com.bwton.socket.transport.SocketController;
import com.bwton.socket.transport.call.Request;
import com.bwton.socket.transport.call.Response;
import com.bwton.socket.transport.call.SocketResponse;
import com.california.pay.socket.ProviderClientService;
import com.california.pay.socket.SocketServerProcessorAdapter;
import com.california.pay.socket.TransSocketMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;


@Slf4j
@SocketController(servicekey = "A1", version = "1.0")
public class ValidateReturnSocket extends SocketServerProcessorAdapter {
    @Autowired
    private ProviderClientService clientService;

    @Override
    public Response doHandle(Request request, Response response) {
        super.doHandle(request, response);
        TransSocketMessage reqMessage = (TransSocketMessage) request.getTms();
        log.info("验证终端有效性返回报文:{}", reqMessage.getSocketBody());
        clientService.callbackRequest(buildResponse(request));
        return response;
    }

    public Response buildResponse(Request request){
        TransSocketMessage reqMessage = (TransSocketMessage)request.getTms();
        TransSocketMessage rspMessage = new TransSocketMessage();

        rspMessage.setAppId(reqMessage.getAppId());
        rspMessage.setVersion(reqMessage.getVersion());
        rspMessage.setSessionId(reqMessage.getSessionId());
        rspMessage.setSeq(reqMessage.getSeq());
        rspMessage.setCommandCode(reqMessage.getCommandCode());
        rspMessage.setTimestamp(reqMessage.getTimestamp());
        rspMessage.setReturnCode(reqMessage.getReturnCode());
        rspMessage.setSignMethod(reqMessage.getSignMethod());
        rspMessage.setSignature(reqMessage.getSignature());
        rspMessage.setMessage(reqMessage.getMessage());
        rspMessage.setSocketBody(reqMessage.getSocketBody());
        Response response = new SocketResponse(rspMessage, request.getRemoteIp());
        return response;
    }



}
