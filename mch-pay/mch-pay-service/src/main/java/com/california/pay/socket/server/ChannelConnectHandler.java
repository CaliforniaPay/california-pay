package com.california.pay.socket.server;

import com.bwton.socket.server.ProviderChannelManageHandler;
import com.california.pay.common.utils.SpringBeanUtils;
import com.california.pay.service.TerminalLoginService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 */
@ChannelHandler.Sharable
public class ChannelConnectHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(ProviderChannelManageHandler.class);
    private TerminalLoginService loginService;

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        if (loginService == null){
            loginService = SpringBeanUtils.getBean(TerminalLoginService.class);
        }
        String  nettyChannelId = ctx.channel().id().asLongText();
        loginService.logout(ctx.channel(), nettyChannelId);
        ctx.fireChannelUnregistered();
    }

}