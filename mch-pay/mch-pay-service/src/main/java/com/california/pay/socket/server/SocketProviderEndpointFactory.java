package com.california.pay.socket.server;

import com.bwton.socket.codec.ProviderRouteMessageHandler;
import com.bwton.socket.log.LoggerUtil;
import com.bwton.socket.server.SocketNettyServer;
import com.bwton.socket.support.SocketServerEndpointInitializer;
import com.bwton.socket.transport.Server;
import com.bwton.socket.transport.URL;

/**
 * netty server 工厂类
 */
public class SocketProviderEndpointFactory extends SocketServerEndpointInitializer {

    @Override
    public Server createServer(URL url) {
        this.url = url;
        LoggerUtil.info(logger, this.getClass().getSimpleName() + "开始创建[SocketNettyServer]服务实例,开始加载相关配置, url={}", url);
        messageHandler = new ProviderRouteMessageHandler(url);
        SocketNettyServer server = new SocketSocketServer(url, messageHandler);
        return server;
    }
}
