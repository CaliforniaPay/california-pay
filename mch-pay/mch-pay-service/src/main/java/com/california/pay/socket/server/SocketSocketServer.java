package com.california.pay.socket.server;

import com.bwton.socket.server.SocketNettyServer;
import com.bwton.socket.transport.URL;
import com.bwton.socket.transport.handler.MessageHandler;
import com.bwton.socket.transport.handler.NettyEncoder;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;

/**
 * 交通行业前置TCP服务端
 *
 * @author liangd.chen
 * @date 2018/8/20
 */
public class SocketSocketServer extends SocketNettyServer {

    private ByteBuf delimiter = Unpooled.copiedBuffer("\r\n".getBytes());

    private ChannelConnectHandler channelConnectHandler = new ChannelConnectHandler();

    public SocketSocketServer(URL url, MessageHandler messageHandler) {
        super(url, messageHandler);
    }

    @Override
    public ChannelHandler getFrameDecoder() {
        return new DelimiterBasedFrameDecoder(5096, delimiter);
    }

    @Override
    public ChannelHandler getProviderDecoder() {
        return new SocketServerDecoder();
    }

    @Override
    public ChannelHandler getProviderEncoder() {
        return new NettyEncoder();
    }

    @Override
    public void afterPipeline(ChannelPipeline pipeline) {
        pipeline.addLast("channelConnectHandler", channelConnectHandler);
    }

    @Override
    public ChannelHandler getServerIdleHandler(URL serverUrl) {
        return new SocketServerIdleHandler(serverUrl);
    }
}
