package com.california.pay.task;

import com.california.pay.common.utils.DateUtils;
import com.california.pay.persist.mapper.PayOrderMapper;
import com.california.pay.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * 检查超时的订单设置为失败
 */
@Slf4j
public class CheckPayOrderFailTask {
    @Autowired
    private PayOrderMapper payOrderMapper;

    public void checkFailPayOrder(){
        BaseService.initTraceId(this.getClass().getSimpleName() + "#personMchOrderPay");
        log.info("开始调度[CheckPayOrderFailTask#checkFailPayOrder]任务，检查订单是否失败。");
        payOrderMapper.updateOrderStatusToFail(Long.parseLong(DateUtils.yyyyMMddHHmmss(new Date())));
        log.info("调度[CheckPayOrderFailTask#checkFailPayOrder]任务完成");
    }
}
