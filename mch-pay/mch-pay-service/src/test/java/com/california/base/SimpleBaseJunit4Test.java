package com.california.base;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
@ContextConfiguration({"classpath:/spring.xml"}) //加载配置文件
@Transactional(transactionManager = "transactionManager", rollbackFor = {Exception.class})
public class SimpleBaseJunit4Test {
}
