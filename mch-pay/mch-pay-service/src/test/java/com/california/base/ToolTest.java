package com.california.base;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author liangd.chen
 * @date 18/6/26
 */
@Slf4j
public class ToolTest {

    @Test
    public void test1() {
        String field_35 = "6224242200000052D280120142990310010F";
        String field_35_rev = build(field_35);
        Assert.assertEquals("24242200000052D28012014299031001", field_35_rev);

        field_35 = "242200000052D280120142990310010F";
        field_35_rev = build(field_35);
        Assert.assertEquals("242200000052D28012014299031001FF", field_35_rev);

        log.info("field_35_rev={}", field_35_rev); //
    }

    public String build(final String field_35){
        String field_35_rev = StringUtils.reverse(field_35);
        field_35_rev = StringUtils.substring(field_35_rev, 2, field_35_rev.length() >= 34 ? 34 : field_35_rev.length());
        field_35_rev = StringUtils.reverse(field_35_rev);
        //System.out.println(field_35_rev);
        String field_35_rev_f = field_35_rev;

        if (field_35_rev_f.length() < 32) {
            field_35_rev_f = StringUtils.rightPad(field_35_rev_f, 32, 'F');
        }

        return field_35_rev;
    }
}
