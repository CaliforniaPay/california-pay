package com.california.pay.facade.impl;

import com.alibaba.fastjson.JSON;
import com.california.base.BaseJunit4Test;
import com.california.pay.facade.MchOrderPayFacade;
import com.california.pay.model.PersonOrderPayReq;
import com.california.pay.model.PersonOrderPayRsp;
import com.california.pay.result.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

/**
 * @author XXXXX
 * @date 2018/12/15
 */
@Slf4j
public class MchOrderPayFacadeImplTest extends BaseJunit4Test{
    @Autowired
    private MchOrderPayFacade mchOrderPayFacade;


    @Test
    public void personMchOrderPayTest() throws Exception {
        PersonOrderPayReq orderPayReq = new PersonOrderPayReq();
        orderPayReq.setInnerMchNo("P200");
        orderPayReq.setAmount(1L);
        orderPayReq.setOutOrderNo("2018121501");
        orderPayReq.setPayType("alipayh5");
        orderPayReq.setGoodsName("good-test");
        orderPayReq.setGoodsDesc("good-test-desc");
        orderPayReq.setClientIp("0.0.0.0");
        orderPayReq.setReturnUrl("http://www.baidu.com/returnUrl");
        orderPayReq.setNotifyUrl("http://www.baidu.com/notifyUrl");
        orderPayReq.setRemark("remark");
        orderPayReq.setSystemTime(LocalDateTime.now());
        CommonResult<PersonOrderPayRsp> commonResult = mchOrderPayFacade.personMchOrderPay(orderPayReq);
        log.info(">>>>>commonResult=" + JSON.toJSONString(commonResult));
    }

    @Test
    public void orderpayNotifyAck() throws Exception {
    }

}