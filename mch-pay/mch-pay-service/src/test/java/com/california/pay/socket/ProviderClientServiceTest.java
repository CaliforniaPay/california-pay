package com.california.pay.socket;

import com.alibaba.fastjson.JSON;
import com.bwton.socket.transport.call.Request;
import com.bwton.socket.transport.call.Response;
import com.bwton.socket.transport.call.SocketRequest;
import com.bwton.socket.util.RemotingUtil;
import com.california.base.BaseJunit4Test;
import com.california.pay.common.utils.MD5Sign;
import com.california.pay.persist.domain.TerminalLogin;
import com.california.pay.service.TerminalLoginService;
import com.google.common.collect.Maps;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;


@Slf4j
public class ProviderClientServiceTest extends BaseJunit4Test {
    @Autowired
    private ProviderClientService providerClientService;
    @Autowired
    private TerminalLoginService loginService;

    @Test
    public void request() throws Exception {
        Thread.sleep(1000);
        String clientid = "295a776758520eef";
        System.out.println(NettyChannelMap.getChannelHashMap());

        TerminalLogin terminalLogin = loginService.getTerminalLogin(clientid);
        TransSocketMessage reqMessage = new TransSocketMessage();

        reqMessage.setAppId("000000");
        reqMessage.setVersion("0001");
        reqMessage.setSessionId(terminalLogin.getSessionId());
        reqMessage.setSeq("000000000000000000");
        reqMessage.setCommandCode("A1");
        reqMessage.setTimestamp(System.currentTimeMillis() + "");
        reqMessage.setReturnCode("00");
        reqMessage.setSignMethod("md5");

        Map<String, String> msgMap = Maps.newHashMap();
        msgMap.put("clientId", clientid);
        msgMap.put("channelUserId", "2088332762557958");
        String message = JSON.toJSONString(msgMap);
        reqMessage.setMessage(message);

        String originBody = reqMessage.getOriginBody();
        String signBody = originBody + "&appKey=" + "tfkCJ97Pp4dytFkeKh6uquknCQ30099j" ;
        String sign = MD5Sign.md5(signBody);
        reqMessage.setSignature(sign);
        reqMessage.setSocketBody(originBody + "&signature=" + sign);

        Channel channel = NettyChannelMap.getChannelByName(terminalLogin.getNettyChannelId());
        Request request = new SocketRequest(reqMessage, RemotingUtil.parseRemoteAddress(channel), channel);

        Response response = providerClientService.request(request, 2000000);
        TransSocketMessage rspMessage = response.getValue();
        System.out.println(rspMessage.getSocketBody());

        System.out.println();

        Thread.currentThread().join();

    }

}