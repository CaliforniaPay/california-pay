package com.california.payopenapi;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.california.payopenapi.socket.Bootstrap;
import com.california.payopenapi.utils.TokensCacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Component;

@Component
@EnableDubbo
@SpringBootApplication
public class PayOpenApiApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(PayOpenApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// 启动websocket服务端
		Bootstrap bootstrap = new Bootstrap();
		bootstrap.start(10001);
	}
}