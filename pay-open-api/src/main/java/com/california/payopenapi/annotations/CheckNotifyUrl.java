package com.california.payopenapi.annotations;

import com.california.payopenapi.validators.CheckNotifyUrlValidator;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @Description: 校验异步通知URL
 * @author: KeyboardMan
 * @Date: Create in 2018-12-05:21:41
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
@Inherited
@Documented
@Constraint(validatedBy = CheckNotifyUrlValidator.class)//指明校验逻辑的类
public @interface CheckNotifyUrl {

    String message() ;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}