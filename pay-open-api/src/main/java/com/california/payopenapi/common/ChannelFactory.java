package com.california.payopenapi.common;

import io.netty.channel.Channel;

import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @Description: 存储连接通道
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:19:50
 */
public class ChannelFactory {

    private static volatile ConcurrentMap<String,Channel> channels = null;

    private ChannelFactory() {}

    public static ConcurrentMap<String,Channel> getInstance() {
        if (channels == null) {
            synchronized (ConcurrentMap.class) {
                if (channels == null) {
                    channels = new ConcurrentHashMap<>();
                }
            }
        }
        return channels;
    }
}
