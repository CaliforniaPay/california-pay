package com.california.payopenapi.common;


import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:20:27
 */
@Slf4j
public class ChannelManager {

    private ChannelManager(){}

    // channelId关联通道编号
    private static Map<String,String> channId_keys = new ConcurrentHashMap<>();

    public static void put(String key, Channel channel) {
        // 通道管理器添加
        ChannelFactory.getInstance().put(key,channel);
        // channelId关联通道编号添加
        channId_keys.put(channel.id().asLongText(),key);
        log.info("通道管理器添加成功  当前在线通道 {} ",ChannelFactory.getInstance().size());
    }

    public static void remove(String key) {
        String channelKey = channId_keys.get(key);
        ChannelFactory.getInstance().remove(channelKey);
        channId_keys.remove(key);
    }

    public static Channel find(String orderNo) {
        return ChannelFactory.getInstance().get(orderNo);
    }

}
