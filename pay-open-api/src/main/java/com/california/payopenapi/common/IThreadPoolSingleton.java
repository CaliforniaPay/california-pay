package com.california.payopenapi.common;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @author:周林锋
 * @Date: Create in 2018-10-11  17:27
 */
public class IThreadPoolSingleton {


    private static volatile ThreadPoolExecutor pool = null;

    private IThreadPoolSingleton(){}

    public static ThreadPoolExecutor getPool() {
        if (pool == null) {
            synchronized (ThreadPoolExecutor.class) {
                if (pool == null) {
                    pool = new IThreadPool(10,
                            50,
                            60L,
                            TimeUnit.SECONDS,
                            new LinkedBlockingQueue<>()

                    );
                }
            }
        }
        return pool;
    }
}