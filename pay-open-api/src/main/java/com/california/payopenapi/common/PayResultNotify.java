package com.california.payopenapi.common;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:22:43
 */
public enum PayResultNotify {

    SUCCESS(1),
    FAIL(0);

    private int code;

    PayResultNotify(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
