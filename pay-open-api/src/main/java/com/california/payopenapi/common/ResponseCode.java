package com.california.payopenapi.common;

/**
 * @Description 定义返回状态码与说明
 * @author 周林锋
 */
public enum ResponseCode{

    SUCCESS(0,"SUCCESS"),
    ERROR(1,"ERROR");

    private final int code;
    private final String desc;


    ResponseCode(int code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public int getCode(){
        return code;
    }


    public String getDesc(){
        return desc;
    }

}
