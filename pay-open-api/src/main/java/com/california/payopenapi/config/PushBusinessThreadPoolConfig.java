package com.california.payopenapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description: 推送业务线程池配置
 * @author: KeyboardMan
 * @Date: Create in 2018-12-24:23:30
 */
@Configuration
public class PushBusinessThreadPoolConfig {

    @Bean
    public ExecutorService executorService() {
        System.out.println("ExecutorService getThreadPool()...");
        return new ThreadPoolExecutor(20, 40, 10, TimeUnit.SECONDS,
                new LinkedBlockingDeque<>());
    }

}
