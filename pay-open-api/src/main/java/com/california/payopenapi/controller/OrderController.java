package com.california.payopenapi.controller;

import com.california.payopenapi.common.ServerResponse;
import com.california.payopenapi.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-25:22:10
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    PayService payService;


    @GetMapping("/status/{orderId}")
    public ServerResponse orderStatus(@PathVariable("orderId") String orderId) {
        if (payService.orderIsSuccess(orderId)) {
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByError();
    }
}
