package com.california.payopenapi.controller;

import com.california.pay.facade.MchOrderNotifyFacade;
import com.california.pay.model.MchOrderPayNotifyMchReq;
import com.california.payopenapi.service.HttpService;
import com.california.payopenapi.service.PushService;
import com.california.payopenapi.utils.MD5Sign;
import com.california.payopenapi.utils.TokensCacheUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-20:0:01
 */
@Slf4j
@Controller
@RequestMapping("/test")
public class TestController {


    @Autowired
    private HttpService httpService;

    @Autowired
    private PushService pushService;


    @Autowired
    TokensCacheUtil cacheUtil;

    @Autowired
    MchOrderNotifyFacade notifyFacade;

    /**
     * 请码测试接口
     * @return
     */
    @RequestMapping(value = "/ttt/{money}", produces = {"text/html"})
    @ResponseBody
    public String sss(@PathVariable("money") long money) {
        String url = httpService.execute(money);
        return url;
    }

    /**
     * 订单同步通知接口
     */
    @RequestMapping("/send")
    public void send() {
        try {
            pushService.send("s123345");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 同步通知访问路径
     * @return
     */
    @RequestMapping("/return")
    @ResponseBody
    public String returnTest() {
        return "go home!!!";
    }

    @RequestMapping("/sendNotify")
    public void sendNotify() {
        MchOrderPayNotifyMchReq req = new MchOrderPayNotifyMchReq();
        req.setInnerOrderNo("s123456");
        req.setChannelUserId("2088012364633491");
        req.setNotifyUrl("http://localhost:10080/test/notify");
        req.setTotalAmount(1L);
        req.setTxnAmount(1L);
        req.setOutOrderNo("dnjwindjin");
        req.setRemark("这是订单备注信息");
        req.setSystemTime(LocalDateTime.now());
        notifyFacade.payOrderNotifyMch(req);
    }



    @RequestMapping(value = "/notify",method = RequestMethod.POST)
    @ResponseBody
    public String AsyncNotify(@RequestBody Map<String,Object> reqMap) throws Exception {
        log.info(reqMap.toString());
        boolean result = MD5Sign.verify(reqMap,"1");
        log.info("验签结果 {}",result);
        return reqMap.get("innerOrderNo").toString();
    }


}
