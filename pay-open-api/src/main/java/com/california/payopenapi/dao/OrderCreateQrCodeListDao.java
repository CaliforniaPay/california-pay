package com.california.payopenapi.dao;

import com.california.payopenapi.entity.OrderCreateQrCodeList;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-29:22:06
 */
@Mapper
public interface OrderCreateQrCodeListDao {

    @Insert("insert into order_create_qrcode_list(order_id,status) values(#{orderId},#{status})")
    int insert(OrderCreateQrCodeList orderCreateQrCodeList);

    @Select("select count(*) from order_create_qrcode_list where order_id = #{orderId}")
    int queryOrderIdIsCreate(String orderId);
}
