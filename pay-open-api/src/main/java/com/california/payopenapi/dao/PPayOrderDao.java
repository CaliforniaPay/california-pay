package com.california.payopenapi.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Description: 订单DAO
 * @author: KeyboardMan
 * @Date: Create in 2018-12-25:22:01
 */
@Mapper
public interface PPayOrderDao {

    @Select("select order_status from p_pay_order where order_id = #{orderId}")
    String queryOrderStats(String orderId);
}
