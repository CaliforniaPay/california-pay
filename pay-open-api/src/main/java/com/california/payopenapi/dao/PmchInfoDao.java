package com.california.payopenapi.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:16:55
 */
@Mapper
public interface PmchInfoDao {


    @Select("select req_key from p_mch_info where mch_no = #{mchNo}")
    String getReqSecret(String mchNo);

    @Select("select res_key from p_mch_info where mch_no = #{mchNo}")
    String getResSecret(String mchNo);
}