package com.california.payopenapi.entity;

import com.california.payopenapi.utils.BeanUtil;
import com.california.payopenapi.utils.JsonUtil;
import com.california.payopenapi.utils.MD5Sign;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.beans.IntrospectionException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * @Description: 支付订单实体
 * @author: KeyboardMan
 * @Date: Create in 2018-12-05:20:41
 */

@Data
@ToString
public class NotifyOrderRequest implements Serializable{

    @NotBlank(message = "商户编号不能为空")
    // 商户编号
    private String innerOrderNo;

    @NotBlank(message = "订单编号不能为空")
    // 订单编号
    private String outOrderNo;

    @Min(value = 1, message = "支付金额不能小于0.01元")
    // 金额
    private Long totalAmount;
    private Long txnAmount;


    private String  remark;
    private String sysTime;

    @NotBlank(message = "签名不能为空")
    @NotNull(message = "请补充签名")
    // MD5 签名串
    private String sign;


    public static void main(String[] args) {
        NotifyOrderRequest payOrderRequest = new NotifyOrderRequest();
        payOrderRequest.setInnerOrderNo("398456056679763969");
        payOrderRequest.setOutOrderNo("190104044034742776729");
        payOrderRequest.setTotalAmount(100L);
        payOrderRequest.setTxnAmount(100L);
        payOrderRequest.setRemark("通知商户消息重试.");
        payOrderRequest.setSysTime("2019-01-04 13:00:00");

        try {
            Map<String, Object> map = BeanUtil.getContext().obj2Map(payOrderRequest);

            payOrderRequest.setSign(MD5Sign.sign(map,"79f1679bacbb883a42babf6c8d4525fc"));
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(JsonUtil.obj2StringPretty(payOrderRequest));
    }
}