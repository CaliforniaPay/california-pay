package com.california.payopenapi.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-29:22:05
 */
@Data
@ToString
@NoArgsConstructor
public class OrderCreateQrCodeList {

    private int id;

    private String orderId;


    // 1: 以生码 2： 未生码
    private int status;
}
