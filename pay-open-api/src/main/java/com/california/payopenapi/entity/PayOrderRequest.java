package com.california.payopenapi.entity;

import com.california.payopenapi.annotations.CheckNotifyUrl;
import com.california.payopenapi.utils.BeanUtil;
import com.california.payopenapi.utils.JsonUtil;
import com.california.payopenapi.utils.MD5Sign;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.*;
import java.beans.IntrospectionException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 支付订单实体
 * @author: KeyboardMan
 * @Date: Create in 2018-12-05:20:41
 */

@Data
@ToString
public class PayOrderRequest implements Serializable{

    @NotBlank(message = "商户编号不能为空")
    // 商户编号
    private String innerMchNo;

    @NotBlank(message = "订单编号不能为空")
    // 订单编号
    private String outOrderNo;

    @Min(value = 1, message = "支付金额不能小于0.01元")
    // 金额
    private Long amount;

    // 支付类型
    private String payType;

    // 商品名称
    private String goodsName;

    // 商品描述
    private String goodsDesc;

    // IP地址
    private String clientIp;

    @CheckNotifyUrl(message = "异步通知地址格式错误")
    // 异步通知URL
    private String notifyUrl;

    @NotBlank(message = "同步回调地址不能为空")
    // 同步通知URL
    private String returnUrl;

    // 订单备注
    private String remark;

    @NotBlank(message = "签名不能为空")
    @NotNull(message = "请补充签名")
    // MD5 签名串
    private String sign;

}