package com.california.payopenapi.exception;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-10:0:36
 */
public class SystemException extends RuntimeException {

    private String errMsg;

    public SystemException(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrMsg() {
        return errMsg;
    }
}
