package com.california.payopenapi.service;

import java.util.Map;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-05:21:56
 */
public interface CheckSignService {

    boolean check(Map params, String secretKey);
}