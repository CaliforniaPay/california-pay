package com.california.payopenapi.service;

import com.california.payopenapi.common.ChannelManager;
import com.california.payopenapi.common.PayResultNotify;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:22:37
 */
@Slf4j
@Service
public class PushService {


    public boolean send(String orderNo) throws Exception{
        log.info("订单结果推送 result:{}",orderNo);
        Channel channel = ChannelManager.find(orderNo);
        if (channel == null) {
            return false;
        }
        channel.writeAndFlush(new TextWebSocketFrame(String.valueOf(PayResultNotify.SUCCESS.getCode())));
        return true;
    }
}