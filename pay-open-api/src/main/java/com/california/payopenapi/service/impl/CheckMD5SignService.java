package com.california.payopenapi.service.impl;

import com.california.payopenapi.service.CheckSignService;
import com.california.payopenapi.utils.MD5Sign;
import org.springframework.stereotype.Service;
import java.util.Map;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-05:21:56
 */
@Service
public class CheckMD5SignService implements CheckSignService{

    @Override
    public boolean check(Map params, String secretKey) {
        boolean signTrue = false;
        try {
            signTrue = MD5Sign.verify(params,secretKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return signTrue;
    }
}
