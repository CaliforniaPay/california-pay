package com.california.payopenapi.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.california.pay.consts.NotifyStatus;
import com.california.pay.facade.MchOrderNotifyFacade;
import com.california.pay.facade.MchOrderPayFacade;
import com.california.pay.model.*;
import com.california.pay.result.CommonResult;
import com.california.payopenapi.common.IThreadPoolSingleton;
import com.california.payopenapi.service.HttpService;
import com.california.payopenapi.service.PushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-11:0:08
 */
@Slf4j
@org.springframework.stereotype.Service
@Service
public class MchOrderNotifyFacadeImpl implements MchOrderNotifyFacade {


    @Autowired
    PushService pushService;

    @Reference
    MchOrderPayFacade mchOrderPayFacade;


    @Autowired
    ExecutorService executorService;

    @Autowired
    HttpService httpService;


    /**
     * 商户平台通知OpenAPI 进行回调
     * @param notifyReq
     * @return
     */
    @Override
    public CommonResult<MchOrderPayNotifyRsp> payOrderNotify(MchOrderPayNotifyReq notifyReq) {
        log.info("订单结果推送 result:{}",notifyReq);
        try {
            pushService.send(notifyReq.getInnerOrderNo());
        } catch (Exception e) {
            log.info("订单结果推送异常  {}",e);
            return CommonResult.failure(e);
        }
        return CommonResult.success();
    }


    /**
     * 商户异步通知接口
     * @param notifyMchReq
     * @return
     */
    @Override
    public CommonResult<MchOrderPayNotifyMchRsp> payOrderNotifyMch(MchOrderPayNotifyMchReq notifyMchReq) {
        log.info("异步通知商户 数据 {}",notifyMchReq);
        final MchOrderPayNotifyMchReq myreq = notifyMchReq;
        executorService.execute(() -> {
            boolean result = httpService.notifyMch(notifyMchReq);
            if (result) {
                try {
                    log.info("发送异步通知 :成功: 回调 ======  start");
                    OrderPayNotifyResultReq resultReq = new OrderPayNotifyResultReq();
                    resultReq.setRemark(myreq.getRemark());
                    resultReq.setSystemTime(LocalDateTime.now());
                    resultReq.setInnerOrderNo(myreq.getInnerOrderNo());
                    resultReq.setChannelUserId(myreq.getChannelUserId());
                    resultReq.setOutOrderNo(myreq.getOutOrderNo());
                    resultReq.setNotifyStatus(NotifyStatus.NOTIFY_SUCC);
                    mchOrderPayFacade.orderpayNotifyAck(resultReq);
                    log.info("发送异步通知 :成功: 回调 ======  end");
                } catch (Exception e) {
                    log.error("调用 mchOrderPayFacade.orderpayNotifyAck(NOTIFY_SUCC)  异常 {}",e);
                }
            } else {
                try {
                    log.info("发送异步通知 :失败: 回调 ======  start");
                    OrderPayNotifyResultReq resultReq = new OrderPayNotifyResultReq();
                    resultReq.setInnerOrderNo(myreq.getInnerOrderNo());
                    resultReq.setOutOrderNo(myreq.getOutOrderNo());
                    resultReq.setSystemTime(LocalDateTime.now());
                    resultReq.setNotifyStatus(NotifyStatus.NOTIFY_FAIL);
                    mchOrderPayFacade.orderpayNotifyAck(resultReq);
                    log.info("发送异步通知 :失败: 回调 ======  end");
                } catch (Exception e) {
                    log.error("调用 mchOrderPayFacade.orderpayNotifyAck(NOTIFY_FAIL)  异常 {} {}",e);
                }
            }
        });
        MchOrderPayNotifyMchRsp mchRsp = new MchOrderPayNotifyMchRsp();
        mchRsp.setSystemTime(LocalDateTime.now());
        return CommonResult.success(mchRsp);
    }
}