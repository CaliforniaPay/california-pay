package com.california.payopenapi.socket;

import lombok.Data;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:20:17
 */
@Data
public class ChannelSocketReq {


    private String key;
}
