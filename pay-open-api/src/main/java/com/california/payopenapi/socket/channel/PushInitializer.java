package com.california.payopenapi.socket.channel;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:19:56
 */
public class PushInitializer extends ChannelInitializer<SocketChannel> {

    NioEventLoopGroup businessGroup = new NioEventLoopGroup();



    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {

        ChannelPipeline pipeline = socketChannel.pipeline();
        //websocket协议本身是基于http协议的，所以这边也要使用http解编码器
        pipeline.addLast(new HttpServerCodec());
        //以块的方式来写的处理器
        pipeline.addLast(new ChunkedWriteHandler());
        //netty是基于分段请求的，HttpObjectAggregator的作用是将请求分段再聚合,参数是聚合字节的最大长度
        pipeline.addLast(new HttpObjectAggregator(8192));
        //参数指的是contex_path
        pipeline.addLast(new WebSocketServerProtocolHandler("/ws"));

        // AuthChannelHandler 这个通道业务 交给authGroup 线程池运行
        pipeline.addLast(businessGroup,new PushHandler());

    }
}
