package com.california.payopenapi.utils;

import com.alibaba.druid.support.json.JSONUtils;
import com.california.payopenapi.vo.AppRequestPayParamsVo;
import org.apache.commons.codec.binary.Base64;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2019-01-09:1:19
 */
public class Base64Util {

    /**
     * 加密
     */
    public static String encode(String plainText){
        byte[] b=plainText.getBytes();
        Base64 base64=new Base64();
        b=base64.encode(b);
        String s=new String(b);
        return s;
    }

    /**
     * 解密
     */
    public static String decode(String encodeStr){
        byte[] b=encodeStr.getBytes();
        Base64 base64=new Base64();
        b=base64.decode(b);
        String s=new String(b);
        return s;
    }


    public static void main(String[] args) {
        AppRequestPayParamsVo payParamsVo = new AppRequestPayParamsVo();
        payParamsVo.setSrc("http://182.23.245.55/gateway/createQucode/34234432435345346456/456546456/6546546546/64666");
        payParamsVo.setAmount(1L);
        payParamsVo.setOrderNo("7345674385853895789589");
        payParamsVo.setReturnUrl("http://182.23.245.55/gateway/createQucode/");
        String jsonStr = JsonUtil.obj2String(payParamsVo);
        String base = encode(jsonStr);

        System.err.println();
        System.err.println();
        System.err.println();
        System.err.println(base);

        String decode = decode(base);
        System.err.println();
        System.err.println();
        System.err.println();
        System.err.println(JsonUtil.string2Obj(decode,AppRequestPayParamsVo.class).toString());
    }
}
