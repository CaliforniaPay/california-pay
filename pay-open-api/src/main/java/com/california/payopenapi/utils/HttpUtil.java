package com.california.payopenapi.utils;

import com.california.payopenapi.entity.PayOrderRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-19:21:27
 */
@Slf4j
public class HttpUtil {

    private HttpUtil(){}

    private static final int SUCCESS_CODE = 200;

    public static String postJson(String url, String content) {
        HttpPost post = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            post = new HttpPost(url);
            // 构造消息头
            post.setHeader("Content-type", "application/json; charset=utf-8");
            // 构建消息实体
            StringEntity entity = new StringEntity(content, Charset.forName("UTF-8"));
            entity.setContentEncoding("UTF-8");
            // 发送Json格式的数据请求
            entity.setContentType("application/json");
            post.setEntity(entity);
            HttpResponse response = httpClient.execute(post);
            log.info("HTTP 执行成功");
            // 检验返回码
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("返回状态码 {}",statusCode);

            if (statusCode == SUCCESS_CODE) {

                String resData = EntityUtils.toString(response.getEntity());

                log.info("异步通知 返回的信息 {}",resData);

                return resData;
            }
            return StringUtils.EMPTY;
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(post != null){
                post.releaseConnection();
            }
        }
        return StringUtils.EMPTY;
    }


    public static String post(long money){
        String url = "";
        HttpPost post = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();

            post = new HttpPost("http://185.239.226.39:10080/gateway/getQR");
            // 构造消息头
            post.setHeader("Content-type", "application/json; charset=utf-8");

            // 构建消息实体
            StringEntity entity = new StringEntity(getEntity(money), Charset.forName("UTF-8"));
            entity.setContentEncoding("UTF-8");
            // 发送Json格式的数据请求
            entity.setContentType("application/json");
            post.setEntity(entity);

            HttpResponse response = httpClient.execute(post);

            return EntityUtils.toString(response.getEntity(), "utf-8");

        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            if(post != null){
                try {
                    post.releaseConnection();
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return url;
    }

    public static String getEntity(long money) {

        PayOrderRequest payOrderRequest = new PayOrderRequest();
        payOrderRequest.setInnerMchNo("P200");
        payOrderRequest.setAmount(money);
        payOrderRequest.setClientIp("192.168.0.32");
        payOrderRequest.setOutOrderNo(System.currentTimeMillis()+"");
        payOrderRequest.setRemark("这是订单备注信息");
        payOrderRequest.setNotifyUrl("http://185.239.226.39:10080/test/notify");
        payOrderRequest.setGoodsName("冬天暖手袋");
        payOrderRequest.setGoodsDesc("限购一件");
        payOrderRequest.setReturnUrl("http://185.239.226.39:10080/test/return?aa=ss&ww=dd");
        payOrderRequest.setPayType("alipay5");

        try {
            Map<String, Object> map = BeanUtil.getContext().obj2Map(payOrderRequest);
            payOrderRequest.setSign(MD5Sign.sign(map,"1"));
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return JsonUtil.obj2String(payOrderRequest);
    }


    public static void main(String[] args) {

    }



}
