package com.california.payopenapi.utils;

import com.california.payopenapi.common.BaseCache;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2019-01-08:14:52
 */
@Component
public class TokensCacheUtil extends BaseCache<String,String> {

    private static final int TOKEN_MAX_SIZE = 2000;

    private static final long TOKEN_EXPIRE_TIME = 242;

    private static final TimeUnit TOKEN_TIME_UNIT = TimeUnit.SECONDS;

    public TokensCacheUtil () {
        super(TOKEN_MAX_SIZE,TOKEN_EXPIRE_TIME,TOKEN_TIME_UNIT);
    }

    @Override
    protected String loadData(String s) {
        return null;
    }

}
