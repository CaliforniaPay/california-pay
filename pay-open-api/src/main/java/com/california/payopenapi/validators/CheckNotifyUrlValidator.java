package com.california.payopenapi.validators;

import com.california.payopenapi.annotations.CheckNotifyUrl;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2018-12-05:21:44
 */
@Component
public class CheckNotifyUrlValidator implements ConstraintValidator<CheckNotifyUrl,String> {

    @Override
    public void initialize(CheckNotifyUrl constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value.contains("?") ? false : true;
    }
}
