package com.california.payopenapi.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Description:
 * @author: KeyboardMan
 * @Date: Create in 2019-01-09:1:14
 */
@Data
@ToString
@NoArgsConstructor
public class AppRequestPayParamsVo {

    private String src;

    private String orderNo;

    private Long amount;

    private String returnUrl;


}
