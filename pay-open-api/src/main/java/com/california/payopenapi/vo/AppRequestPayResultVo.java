package com.california.payopenapi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Description: APP 对接方请码返回实体
 * @author: KeyboardMan
 * @Date: Create in 2019-01-09:1:07
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AppRequestPayResultVo {


    private String url;

}
