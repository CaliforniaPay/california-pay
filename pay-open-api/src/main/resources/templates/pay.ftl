<!DOCTYPE html>
<html lang="en">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta name="renderer" content="webkit" />
    <title>CaliforniaPay -- 轻松支付</title>
    <link rel="stylesheet" media="screen"  href="https://cdn.fastadmin.net/assets/addons/pay/css/pay.css?v=?v=1.0.11"></head>
    <link rel="stylesheet" media="screen" href="https://cdn.fastadmin.net/assets/libs/font-awesome/css/font-awesome.min.css?v=?v=1.0.11"/>
    <!--[if lt IE 9]>
    <script src="https://cdn.fastadmin.net/libs/html5shiv.js?v=?v=1.0.11"></script>
    <script src="https://cdn.fastadmin.net/libs/respond.min.js?v=?v=1.0.11"></script>
    <![endif]-->
    </head>
    <body>
    <div class="container">

        <div class="mainbody">
            <div class="realprice" >￥${amount}</div>
            <p class="tips" style="color:red">
                支付方式1：截屏保存二维码到相册,打开支付宝扫相册中的二维码付款<br/>
                支付方式2：用另一台手机扫这个二维码付款
            </p>

            <div class="qrcode">
                <img class="image" src="${src}" >
                <div class="logo hidden-xs logo-alipay"></div>
                <div class="expired hidden"></div>
                <div class="paid hidden"></div>
            </div>
            <div class="remainseconds">
                <div class="time minutes">
                    <b></b>
                    <em>分</em>
                </div>
                <div class="colon">:</div>
                <div class="time seconds">
                    <b></b>
                    <em>秒</em>
                </div>
            </div>

            <br>
             订单号:<b>${orderNo}</b>

            <br>            <br>


            <div class="tips">打开支付宝「扫一扫」</div>
        </div>
        <script type="text/javascript" src="https://cdn.fastadmin.net/assets/libs/jquery/dist/jquery.min.js?v=1.0.11"></script>
        <script>
            $(function () {

                var timer, minutes, seconds, ci, qi;
                timer = parseInt(240) - 1;
                ci = setInterval(function () {
                    minutes = parseInt(timer / 60, 10)
                    seconds = parseInt(timer % 60, 10);
                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    $(".minutes b").text(minutes);
                    $(".seconds b").text(seconds);
                    if (--timer < 0) {
                        $(".qrcode .expired").removeClass("hidden");
                        clearInterval(ci);
                        clearInterval(qi);
                        $(".tips").html("支付超时.... 正在查询订单状态")
                        checkOrderStats()
                    }
                }, 1000);


                var orderNo = "${orderNo}";
                var returnUrl = "${returnUrl}";

                var websocket = null;

                //判断当前浏览器是否支持WebSocket
                if ('WebSocket' in window) {
                    websocket = new WebSocket("ws://185.239.226.39:10001/ws");
                } else{
                    alert('Not support websocket')
                }
                //连接发生错误的回调方法
                websocket.onerror = function(){

                };
                //连接成功建立的回调方法
                websocket.onopen = function(event){
                    var data = JSON.stringify({
                        "key":orderNo
                    });
                    websocket.send(data);
                };

                //接收到消息的回调方法
                websocket.onmessage = function(event){
                    if (event.data == '1') {
                        websocket.close();
                        window.location.href=returnUrl
                    }
                };

                //连接关闭的回调方法
                websocket.onclose = function(){
                    setMessageInnerHTML("close");
                };

                //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
                window.onbeforeunload = function(){
                    websocket.close();
                };
                //关闭连接
                function closeWebSocket(){
                    websocket.close();
                }

                function checkOrderStats() {
                    $.ajax({
                        url:'/order/status/'+orderNo,
                        type:'GET',
                        async: false,
                        success:function (res) {
                            if (res.status == 0) {
                                websocket.close();
                                window.location.href=returnUrl
                            } else {
                                $(".tips").html("支付失败, 请重新支付")
                            }
                        }
                    })
                }


                function checkOrderStats() {
                    $.ajax({
                        url:'http://185.239.226.39:10080/order/status/'+orderNo,
                        type:'GET',
                        async: false,
                        success:function (res) {
                            if (res.code == 0) {
                                websocket.close();
                                window.location.href=returnUrl
                            } else {
                                $(".tips").html("支付失败  请重新支付")
                            }
                        }
                    })
                }
            });

        </script>
    </div>
    </body>
</html>